Imports System.Reflection
Imports System.Resources

<Assembly: AssemblyCompany("Integrated Scientific Resources")>
<Assembly: AssemblyCopyright("(c) 2018 Invenietis. All rights reserved")>
<Assembly: AssemblyTrademark("Licensed under The MIT License.")>
<Assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)>
<Assembly: AssemblyCulture("")>

