using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.UnitsOfMeasure
{
    partial struct ExpFactor
    {

#pragma warning disable 1591
        public static bool operator ==( ExpFactor left, ExpFactor right ) => left.Equals( right );
        public static bool operator !=( ExpFactor left, ExpFactor right ) => !left.Equals( right );
        public static bool operator >( ExpFactor left, ExpFactor right ) => left.CompareTo( right ) > 0;
        public static bool operator <( ExpFactor left, ExpFactor right ) => left.CompareTo( right ) < 0;
        public static bool operator >=( ExpFactor left, ExpFactor right ) => left.CompareTo( right ) >= 0;
        public static bool operator <=( ExpFactor left, ExpFactor right ) => left.CompareTo( right ) <= 0;
#pragma warning restore 1591

    }
}
