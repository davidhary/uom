using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.UnitsOfMeasure
{
    public partial struct Quantity : IFormattable
    {

        #region . Modifications .
#if false
        /// <summary>
        /// The value.
        /// </summary>
        public double Value { get; }

        readonly MeasureUnit _unit;
        /// <summary>
        /// The unit of measure of the <see cref="Value"/>.
        /// Never null.
        /// </summary>
        public MeasureUnit Unit => _unit ?? MeasureUnit.None;

        string _normalized;
        /// <summary> Gets the normalized. </summary>
        ///
        /// <value> The normalized. </value>
        public string Normalized { get { return _normalized; }  }

        /// <summary>
        /// Initializes a new <see cref="Quantity"/>.
        /// </summary>
        /// <param name="v">Quantity value.</param>
        /// <param name="unit">Unit of measure. Can be null (<see cref="MeasureUnit.None"/> is used).</param>
        public Quantity( double v, MeasureUnit unit )
        {
            this.Value = v;
            this._unit = unit ?? MeasureUnit.None;
            this._normalized = null;
        }

#endif
#endregion Modifications

#region . To String .

        public const string FormatGeneralUnitName = "GN";
        public const string FormatGeneralUnitSymbol = "GS";
        public const string FormatNumericUnitName = "NN";
        public const string FormatNumericUnitSymbol = "NS";
        public const string FormatCustomUnitName = "UN";
        public const string FormatCustomUnitSymbol = "US";

        /// <summary>
        /// Shows a string representation of the quantity, formatted according to the passed format
        /// string, using the given format provider.
        /// </summary>
        ///
        /// <remarks>
        /// Valid format strings are:<para>
        /// <list type="bullet"><item>
        /// '{1:numeric format} {2}' or </item><item>
        /// '{1:numeric format} {2:N}' or </item><item>
        /// '{1:numeric format} {2:S}' </item></list>
        /// for a custom numeric format followed by the unit symbol {2}, name {2:N} or symbol
        /// {2:S}.</para> or <para>
        /// 'GN', 'GS' [Default], 'NN', 'NS' </para> or <para>
        /// where the first letter represents the value formatting (General, Numeric), and the second
        /// letter represents the unit formatting (General, Name, Symbol)), </para> or <para>
        /// A custom number format with 'UG', 'UN' or 'US' (UnitGeneral, UnitName or UnitSymbol)
        /// representing the unit (i.e. "#,##0.00 US").</para><para>  The format string can also contains
        /// a '|' followed by a unit to convert to </para>.
        /// </remarks>
        ///
        /// <param name="format">           The format to use.-or- A null reference (Nothing in Visual
        ///                                 Basic)
        ///                                 to use the default format defined for the type of the
        ///                                 <see cref="T:System.IFormattable" />
        ///                                 implementation. </param>
        /// <param name="formatProvider">   The provider to use to format the value.-or- A null reference
        ///                                 (Nothing in Visual Basic) to obtain the numeric format
        ///                                 information from the current locale setting of the operating
        ///                                 system. </param>
        ///
        /// <returns>   A string that represents this object. </returns>

        public string ToString( string format, IFormatProvider formatProvider )
        {
            if( format == null ) format = Quantity.FormatGeneralUnitSymbol;

            if( formatProvider != null )
            {
                if( formatProvider.GetFormat( this.GetType() ) is ICustomFormatter formatter )
                {
                    return formatter.Format( format, this, formatProvider );
                }
            }

            String[] formats = format.Split( '|' );
            Quantity quantity = this;
            if( formats.Length >= 2 )
            {
                if( formats[1] == "?" )
                    quantity = quantity.ConvertTo( quantity.Unit);
                else
                    quantity = quantity.ConvertTo( UnitRegister.GetUnitByName( formats[1]) );
            }

            format = formats[0];
            if( format.StartsWith( "{", StringComparison.OrdinalIgnoreCase ) )
            {
                if( format.Split( ' ' ).Length == 2 )
                    return String.Format( formatProvider, format, quantity.Value, quantity.Unit ).TrimEnd( null );
                else
                    return String.Format( formatProvider, format, quantity.Value ).TrimEnd( null );
            }
            else
            {
                switch( format )
                {
                    case Quantity.FormatGeneralUnitName:
                        return String.Format( formatProvider, "{0:G} {1:N}", quantity.Value, quantity.Unit ).TrimEnd( null );
                    case Quantity.FormatGeneralUnitSymbol:
                        return String.Format( formatProvider, "{0:G} {1:S}", quantity.Value, quantity.Unit ).TrimEnd( null );
                    case Quantity.FormatNumericUnitName:
                        return String.Format( formatProvider, "{0:N} {1:N}", quantity.Value, quantity.Unit ).TrimEnd( null );
                    case Quantity.FormatNumericUnitSymbol:
                        return String.Format( formatProvider, "{0:N} {1:S}", quantity.Value, quantity.Unit ).TrimEnd( null );
                    default:
                        formats[0] = formats[0].Replace( Quantity.FormatCustomUnitName, "\"" + quantity.Unit.ToString( MeasureUnit.UnitNameFormatCode , formatProvider ) + "\"" );
                        formats[0] = formats[0].Replace( Quantity.FormatCustomUnitSymbol, "\"" + quantity.Unit.ToString( MeasureUnit.UnitSymbolFormatCode, formatProvider ) + "\"" );
                        return quantity.Value.ToString( formats[0], formatProvider ).TrimEnd( null );
                }
            }
        }

        /// <summary>
        /// Static convenience ToString method, returns ToString of the quantity, or empty string if
        /// quantity is null.
        /// </summary>
        ///
        /// <param name="format">   The format to use.-or- A null reference (Nothing in Visual Basic)
        ///                         to use the default format defined for the type of the
        ///                         <see cref="T:System.IFormattable" />
        ///                         implementation. </param>
        ///
        /// <returns>   A string that represents this object. </returns>

        public string ToString( string format )
        {
            return ToString( format , (IFormatProvider)null );
        }


        /// <summary> Static convenience ToString method, returns ToString of the quantity, or empty string
        /// if quantity is null. </summary>
        /// <param name="quantity"> The quantity. </param>
        /// <returns> A string that represents this object. </returns>
        public static string ToString( Quantity quantity )
        {
            return ToString( quantity, (string)null, (IFormatProvider)null );
        }

        /// <summary> Static convenience ToString method, returns ToString of the quantity, or empty string
        /// if quantity is null. </summary>
        /// <param name="quantity"> The quantity. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <returns> A string that represents this object. </returns>
        public static string ToString( Quantity quantity, string format )
        {
            return ToString( quantity, format, (IFormatProvider)null );
        }

        /// <summary> Static convenience ToString method, returns ToString of the quantity, or empty string
        /// if quantity is null. </summary>
        /// <param name="quantity">         The quantity. </param>
        /// <param name="formatProvider"> The format provider. </param>
        /// <returns> A string that represents this object. </returns>
        public static string ToString( Quantity quantity, IFormatProvider formatProvider )
        {
            return ToString( quantity, (string)null, formatProvider );
        }

        /// <summary> Static convenience ToString method, returns ToString of the quantity, or empty string
        /// if quantity is null. </summary>
        /// <param name="quantity">         The quantity. </param>
        /// <param name="format">         Describes the format to use. </param>
        /// <param name="formatProvider"> The format provider. </param>
        /// <returns> A string that represents this object. </returns>
        public static string ToString( Quantity quantity, string format, IFormatProvider formatProvider )
        {
            return quantity.ToString( format, formatProvider );
        }

#endregion To String

#region . Static Operations .

        /// <summary>
        /// Multiplies two quantities. This is always possible: the resulting quantity's unit will hold
        /// the combined unit.
        /// </summary>
        ///
        /// <param name="sourceQuantity">       A Quantity to process. </param>
        /// <param name="multiplyingQuantity">  The multiplying quantity. </param>
        ///
        /// <returns>   The resulting quantity. </returns>
        public static Quantity Multiply( Quantity sourceQuantity , Quantity multiplyingQuantity )
        {
            if ((multiplyingQuantity.Value != 0) &&
                MeasureUnit.AreCanonical(sourceQuantity.Unit, multiplyingQuantity.Unit.Invert()) )
            {
                // if the source and inverted target units are convertible, the outcome is dimensionless
                // convert the inverted quantity to the unit of the source quantity and then divide.
                Quantity inverted = multiplyingQuantity.Invert().ConvertTo( sourceQuantity.Unit );
                return new Quantity( sourceQuantity.Value / inverted.Value, sourceQuantity.Unit / inverted.Unit );
            }
            else return new Quantity( sourceQuantity.Value * multiplyingQuantity.Value, sourceQuantity.Unit * multiplyingQuantity.Unit );
        }

        /// <summary>
        /// Multiplies two quantities. This is always possible: the resulting quantity's unit will hold
        /// the combined unit.
        /// </summary>
        ///
        /// <param name="quantity"> The quantity to divide. </param>
        /// <param name="factor">   The factor. </param>
        ///
        /// <returns>   The resulting quantity. </returns>

        public static Quantity Multiply( Quantity quantity, double factor ) => new Quantity( quantity.Value * factor, quantity.Unit );

        /// <summary>
        /// Multiplies two quantities. This is always possible: the resulting quantity's unit will hold
        /// the combined unit.
        /// </summary>
        ///
        /// <param name="factor">   The factor. </param>
        /// <param name="quantity"> The quantity to divide. </param>
        ///
        /// <returns>   The resulting quantity. </returns>

        public static Quantity Multiply( double factor, Quantity quantity ) => new Quantity( quantity.Value * factor, quantity.Unit );

        /// <summary>
        /// Divides a quantity with another one. This is always possible: the resulting quantity's
        /// unit will hold the combined unit.
        /// </summary>
        ///
        /// <param name="numerator">     The quantity to divide. </param>
        /// <param name="denominator">  The denominator. </param>
        ///
        /// <returns>   The resulting quantity. </returns>
        public static Quantity Divide( Quantity numerator, Quantity denominator )
        {
            if (MeasureUnit.AreCanonical( numerator.Unit, denominator.Unit ) )
            {
                // if the numerator and denominator units are convertible, the outcome is dimensionless
                // convert the denominator to the numerator units thus getting dimensionless outcome.
                Quantity converted = denominator.ConvertTo( numerator.Unit );
                return new Quantity( numerator.Value / converted.Value, numerator.Unit / converted.Unit );
            }
            else return new Quantity( numerator.Value / denominator.Value, numerator.Unit / denominator.Unit );
        }

        /// <summary>
        /// Divides a quantity by a value.
        /// </summary>
        ///
        /// <param name="quantity">     The quantity to divide. </param>
        /// <param name="denominator">  The denominator. </param>
        ///
        /// <returns>   The resulting quantity. </returns>

        public static Quantity Divide( Quantity quantity, double denominator ) => new Quantity( quantity.Value / denominator, quantity.Unit );

        /// <summary>
        /// Divides a value by a quantity.
        /// </summary>
        ///
        /// <param name="numerator">    The numerator. </param>
        /// <param name="quantity">     The quantity to divide by. </param>
        ///
        /// <returns>   The resulting quantity. </returns>

        public static Quantity Divide( double numerator, Quantity quantity ) => new Quantity( numerator / quantity.Value, quantity.Unit.Invert() );

        /// <summary>
        /// Subtracts one quantity from another, returning a quantity with this
        /// <see cref="Quantity.Unit"/>. The quantity to subtract must be convertible into the
        /// <see cref="Quantity.Unit"/> to subtract from such that (<see cref="CanAdd(Quantity)"/> must
        /// be true) otherwise an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        ///
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        ///
        /// <returns>   A Quantity. </returns>

        public static Quantity Subtract( Quantity left, Quantity right ) => Add( left, right.Negate() );

        /// <summary>
        /// Inverts a quantity. The result' value is 1/<see cref="Quantity.Value"/> and its <see cref="Quantity.Unit"/>
        /// is <see cref="MeasureUnit.Invert"/>.
        /// </summary>
        ///
        /// <param name="quantity">    The quantity to invert. </param>
        ///
        /// <returns>   The inverse quantity. </returns>

        public static Quantity Invert( Quantity quantity ) => new Quantity( 1.0 / quantity.Value, quantity.Unit.Invert() );

        /// <summary>   Elevates a quantity by a given power. </summary>
        ///
        /// <param name="quantity"> The quantity to elevate. </param>
        /// <param name="exp">      The exponent. </param>
        ///
        /// <returns>   The resulting quantity. </returns>

        public static Quantity Power( Quantity quantity , int exp ) => new Quantity( Math.Pow( quantity.Value, exp ), quantity.Unit.Power( exp ) );

        /// <summary>
        /// Checks whether another quantity can be added to this one. The added quantity must be
        /// convertible (see <see cref="CanConvertTo(MeasureUnit)"/>) into this <see cref="Quantity.Unit"/>.
        /// </summary>
        ///
        /// <param name="sourceQuantity">   A Quantity to process. </param>
        /// <param name="addedQuantity">    The quantity that may be added to this quantity. </param>
        ///
        /// <returns>   True if <see cref="Add(Quantity)"/> can be called. </returns>

        public static bool CanAdd( Quantity sourceQuantity, Quantity addedQuantity ) => addedQuantity.CanConvertTo( sourceQuantity.Unit );

        /// <summary>
        /// Adds a given quantity to this one, returning a quantity with this <see cref="Quantity.Unit"/>. The
        /// quantity to add must be convertible into this <see cref="Quantity.Unit"/> (<see cref="CanAdd(Quantity)"/> must
        /// be true)
        /// otherwise an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        ///
        /// <param name="sourceQuantity">   A Quantity to process. </param>
        /// <param name="addedQuantity">    The quantity to add. </param>
        ///
        /// <returns>   A quantity with this <see cref="Unit"/>. </returns>

        public static Quantity Add(Quantity sourceQuantity, Quantity addedQuantity )
        {
            if( addedQuantity.Unit == sourceQuantity.Unit ) return new Quantity( sourceQuantity.Value + addedQuantity.Value, sourceQuantity.Unit );
            var qC = addedQuantity.ConvertTo( sourceQuantity.Unit );
            return new Quantity( sourceQuantity.Value + qC.Value, sourceQuantity.Unit );
        }

        /// <summary>
        /// Negates a quantity: it is the negated <see cref="Quantity.Value"/> with the same <see cref="Quantity.Unit"/>.
        /// </summary>
        /// <returns>The negated quantity.</returns>
        public static Quantity Negate( Quantity sourceQuantity ) => new Quantity( -sourceQuantity.Value, sourceQuantity.Unit );

        #endregion static operations

        #region . Equality and Comparison .

        /// <summary>
        /// Returns the string representation of this quantity in this <see cref="Unit"/>'s <see cref="MeasureUnit.Normalization"/>
        /// unit and <see cref="CultureInfo.InvariantCulture"/>.
        /// </summary>
        /// <returns>A readable string.</returns>
        public static string ToNormalizedString( Quantity sourceQuantity )
        {
            if( sourceQuantity.Normalized == null )
            {
                return sourceQuantity.Unit == null ? "0" : sourceQuantity.ConvertTo( sourceQuantity.Unit.Normalization ).ToString( CultureInfo.InvariantCulture );
            }
            else { return sourceQuantity.Normalized; }
        }

        /// <summary>
        /// Supports Unit-aware equality. See <see cref="Equals(Quantity)"/>.
        /// </summary>
        ///
        /// <param name="sourceQuantity">    Quantity to be compared. </param>
        /// <param name="obj">              The object to test. </param>
        ///
        /// <returns>   True if this quantity is the same as the one, false otherwise. </returns>

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "obj" )]
        public static bool Equals( Quantity sourceQuantity, object obj ) => obj is Quantity q && Equals ( sourceQuantity, q );

        /// <summary>
        /// Support for Unit-aware equality.
        /// </summary>
        /// <returns>The hash code to use for this quantity.</returns>
        public static int GetHashCode( Quantity quantity ) => quantity.Unit.Normalization.GetHashCode() ^ quantity.ToNormalizedString().GetHashCode();

        /// <summary>
        /// Checks if this quantity is equal to another one: its
        /// <see cref="Quantity.ToNormalizedString(Quantity)"/>
        /// is the same as the other quantity (and they belong to the same <see cref="MeasureContext"/>).
        /// </summary>
        ///
        /// <param name="sourceQuantity">    Quantity to be compared. </param>
        /// <param name="other">            The quantity that may be equal to this. </param>
        ///
        /// <returns>   True if this quantity is the same as the other one, false otherwise. </returns>

        public static bool Equals( Quantity sourceQuantity, Quantity other ) => sourceQuantity.Unit.Context == other.Unit.Context
                                                && sourceQuantity.ToNormalizedString() == other.ToNormalizedString();

        /// <summary> Compares a quantity to another one. </summary>
        ///
        /// <param name="sourceQuantity">   A Quantity to process. </param>
        /// <param name="other">            The other quantity to compare. </param>
        ///
        /// <returns>
        /// Negative if this object is less than the other, 0 if they are equal, or positive if this is
        /// greater.
        /// </returns>
        public static int CompareTo( Quantity sourceQuantity, Quantity other )
        {
            var tU = sourceQuantity.Unit;
            var oU = other.Unit;
            // Do the 2 units belong to the same (possibly null) context?
            if( tU.Context == oU.Context )
            {
                // First check our equality: we must do this first to ensure coherency.
                if( Quantity.ToNormalizedString( sourceQuantity ) == other.ToNormalizedString() )
                {
                    return 0;
                }
                // Same unit, we compare the Values.
                if( tU == oU )
                {
                    return sourceQuantity.Value.CompareTo( other.Value );
                }
                // Same normalized units, we convert this Value to the other unit before comparison.
                if( tU.Normalization == oU.Normalization )
                {
                    FullFactor ratio = tU.NormalizationFactor.DivideBy( oU.NormalizationFactor );
                    return (sourceQuantity.Value * ratio.ToDouble()).CompareTo( other.Value );
                }
                // Inverted normalized units, we convert this Value to the other unit before comparison.
                if( tU.Normalization == oU.Normalization.Invert() )
                {
                    FullFactor ratio = tU.NormalizationFactor.Multiply( oU.NormalizationFactor );
                    return (1 / (sourceQuantity.Value * ratio.ToDouble())).CompareTo( other.Value );
                }
                // No possible conversion. How to compare kilograms and milliSievert?
                // Using their symbol (here kilogram will be "smaller" than milliSievert)
                return string.Compare( tU.Symbol, oU.Symbol, true, System.Globalization.CultureInfo.CurrentCulture );
            }
            // Not in the same context.
            if( tU == MeasureUnit.None ) return -1;
            if( oU == MeasureUnit.None ) return 1;
            return string.Compare( tU.Context.Name, oU.Context.Name, true, System.Globalization.CultureInfo.CurrentCulture );
        }

#endregion Equality

#region . Casting .

        /// <summary>
        /// Checks whether this quantity can be converted into a quantity with a different
        /// <see cref="MeasureUnit"/>.
        /// </summary>
        ///
        /// <param name="targetUnit">   The target unit. </param>
        ///
        /// <returns>
        /// True if this quantity can be expressed in the target unit, false otherwise.
        /// </returns>
        public bool CanConvertTo( MeasureUnit targetUnit )
        {
            return MeasureUnit.AreConvertible( this.Unit, targetUnit );
        }

        /// <summary> Casts a double value to an Quantity expressed in the None unit. </summary>
        /// <param name="value"> The raw value of the Quantity. </param>
        public static explicit operator Quantity( double value )
        {
            return new Quantity( value, MeasureUnit.None );
        }

        /// <summary>   Casts an Quantity expressed in the None unit to a double. </summary>
        ///
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        ///
        /// <param name="quantity"> The Quantity to convert. </param>
        ///
        /// <returns>   The result of the operation. </returns>

        public static explicit operator double ( Quantity quantity )
        {
            if( quantity.CanConvertTo( MeasureUnit.None ) )
            {
                return quantity.ConvertTo( MeasureUnit.None ).Value; 
            }
            else
            {
                throw new InvalidCastException( $"An Quantity can only be cast to a numeric type if it is expressed in a {nameof( MeasureUnit.None )} unit." );
            }
        }

#endregion Casting 

#region . Conversions .

        /// <summary>
        /// Returns a matching quantity converted to the given unit and rounded up to the given number of
        /// decimals or truncated if the decimal count is negative.
        /// </summary>
        ///
        /// <param name="unit">     The unit of the amount. </param>
        /// <param name="decimals"> The decimals. </param>
        ///
        /// <returns>   A quantity. </returns>
        public Quantity ConvertTo( MeasureUnit unit, int decimals )
        {
            if( decimals >= 0 )
            {
                return new Quantity( Math.Round( this.ConvertTo( unit ).Value, decimals ), unit );
            }
            else
            {
                return new Quantity( Math.Truncate( this.ConvertTo( unit ).Value ), unit );
            }
        }

        /// <summary>
        /// Converts the source quantity to another <see cref="MeasureUnit"/>. Must be called only if
        /// <see cref="Quantity.CanConvertTo(MeasureUnit)"/> returned true otherwise an
        /// <see cref="ArgumentException"/> is thrown.
        /// </summary>
        ///
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        ///
        /// <param name="sourceQuantity">   A Quantity to process. </param>
        /// <param name="targetUnit">       The target unit of measure. </param>
        ///
        /// <returns>   The quantity expressed with the target unit. </returns>
        public static Quantity Convert( Quantity sourceQuantity, MeasureUnit targetUnit )
        {
            if( sourceQuantity == null ) throw new ArgumentNullException( nameof( sourceQuantity ) );
            if( targetUnit == null ) throw new ArgumentNullException( nameof( targetUnit ) );
            if( sourceQuantity.Unit == targetUnit ) return sourceQuantity;
            if( !sourceQuantity.CanConvertTo( targetUnit ) )
            {
                if( targetUnit.Context != sourceQuantity.Unit.Context )
                    throw new ArgumentException( $"Can not convert between units in different contexts ('{sourceQuantity.Unit}'@'{sourceQuantity.Unit.Context}'  to '{targetUnit}'@'{targetUnit.Context}')." );
                throw new ArgumentException( $"Can not convert from '{sourceQuantity.Unit}' to '{targetUnit}'." );
            }
            if( sourceQuantity.Unit.Normalization == targetUnit.Normalization )
            {
                FullFactor ratio = sourceQuantity.Unit.NormalizationFactor.DivideBy( targetUnit.NormalizationFactor );
                return new Quantity( sourceQuantity.Value * ratio.ToDouble(), targetUnit );
            }
            else
            {
                FullFactor ratio = sourceQuantity.Unit.NormalizationFactor.Multiply( targetUnit.NormalizationFactor );
                return new Quantity( 1 / (sourceQuantity.Value * ratio.ToDouble()), targetUnit );
            }
        }

        #endregion Conversions

        #region . Splitting .

        /// <summary>
        /// Splits this quantity into integral values of the given units except for the last quantity which
        /// is rounded up to the number of decimals given.
        /// </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="units">    The units. </param>
        /// <param name="decimals"> The decimals. </param>
        ///
        /// <returns>   A Quantity[]. </returns>

        public Quantity[] Split( MeasureUnit[] units, int decimals )
        {
            if( units == null ) throw new ArgumentNullException( nameof( units ) );
            Quantity[] quantities = new Quantity[units.Length];
            Quantity rest = this;

            // Truncate for all but the last unit:
            for( int i = 0; i < (units.Length - 1); i++ )
            {
                quantities[i] = (Quantity)rest.ConvertTo( units[i] , -1 ).MemberwiseClone();
                rest = rest - quantities[i];
            }

            // Handle the last unit:
            quantities[units.Length - 1] = rest.ConvertTo( units[units.Length - 1], decimals );

            return quantities;
        }

        #endregion

    }
}
