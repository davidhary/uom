using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.UnitsOfMeasure
{
    public partial class MeasureUnit : IFormattable
    {

#region . Constants .

/// <summary>   The none symbol. </summary>
public const string NoneSymbol = "";

        #endregion

        #region . Static Operations .

        /// <summary>
        /// Checks whether the two <see cref="MeasureUnit"/>s are canonical. That is they share the
        /// same <see cref="MeasureUnit.Normalization"/> and thus can be converted.
        /// </summary>
        ///
        /// <param name="sourceUnit">   Source unit. </param>
        /// <param name="targetUnit">   Target unit. </param>
        ///
        /// <returns>
        /// True if a quantity with the source <see cref="MeasureUnit"/> can be expressed in the target
        /// unit, false otherwise.
        /// </returns>

        public static bool AreCanonical( MeasureUnit sourceUnit, MeasureUnit targetUnit )
        {
            if( sourceUnit == null ) throw new ArgumentNullException( nameof( sourceUnit ) );
            if( targetUnit == null ) throw new ArgumentNullException( nameof( targetUnit ) );
            return sourceUnit == targetUnit ||
               ((targetUnit == MeasureUnit.None) || (sourceUnit == MeasureUnit.None) || (sourceUnit.Context == targetUnit.Context)) &&
                    (sourceUnit.Normalization == targetUnit.Normalization);
        }

        /// <summary>
        /// Checks whether the two <see cref="MeasureUnit"/>s are canonical or inverse canonical. That is they share the
        /// same or inverse <see cref="MeasureUnit.Normalization"/> and thus can be converted.
        /// </summary>
        /// <returns>True if a quantity with the source <see cref="MeasureUnit"/> can be expressed in the target unit, false otherwise.</returns>
        public static bool AreConvertible( MeasureUnit sourceUnit , MeasureUnit targetUnit )
        {
            if( sourceUnit == null ) throw new ArgumentNullException( nameof( sourceUnit ) );
            if( targetUnit == null ) throw new ArgumentNullException( nameof( targetUnit ) );
            return sourceUnit == targetUnit ||
                (( targetUnit == MeasureUnit.None ) || (sourceUnit == MeasureUnit.None) || (sourceUnit.Context == targetUnit.Context)) &&
                (sourceUnit.Normalization == targetUnit.Normalization || sourceUnit.Normalization == targetUnit.Normalization.Invert());
        }

        /// <summary>   Divides. </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="numerator">    The numerator. </param>
        /// <param name="denominator">  The denominator. </param>
        ///
        /// <returns>   A MeasureUnit. </returns>

        public static MeasureUnit Divide( MeasureUnit numerator, MeasureUnit denominator )
        {
            if( numerator == null ) throw new ArgumentNullException( nameof( numerator ) );
            if( denominator == null ) throw new ArgumentNullException( nameof( denominator ) );
            return numerator.DivideBy( denominator );
        }

        /// <summary>
        /// Returns the <see cref="MeasureUnit"/> that results from this one multiplied by another one.
        /// </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="left">     The left. </param>
        /// <param name="right">    The right. </param>
        ///
        /// <returns>   The result of the multiplication. </returns>

        public static MeasureUnit Multiply( MeasureUnit left, MeasureUnit right )
        {
            if( left == null ) throw new ArgumentNullException( nameof( left ) );
            if( right == null ) throw new ArgumentNullException( nameof( right ) );
            return left.Multiply( right );
        }

        /// <summary>
        /// Returns this measure of units elevated to a given power. Note that when
        /// <paramref name="exp"/> is 0, <see cref="None"/> is returned.
        /// </summary>
        ///
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        ///
        /// <param name="value">    The value. </param>
        /// <param name="exp">      The exponent. </param>
        ///
        /// <returns>   The resulting normalized units. </returns>

        public static MeasureUnit Power( MeasureUnit value, int exp )
        {
            if( value == null ) throw new ArgumentNullException( nameof( value ) );
            return value.Power( exp );
        }

#endregion Static Operations

#region . Representation (To String) .

        /// <summary>   The unit name format code. </summary>
        public const string UnitNameFormatCode = "N";

        /// <summary>   The unit symbol format code. </summary>
        public const string UnitSymbolFormatCode = "S";

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        public override string ToString()
        {
            return this.ToString( null, null );
        }

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        /// <remarks>
        /// The format string can be either 'N' (Unit Name) or 'S' (Unit Symbol).
        /// </remarks>
        public string ToString( string format )
        {
            return this.ToString( format, null );
        }

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        public string ToString( IFormatProvider formatProvider )
        {
            return this.ToString( null, formatProvider );
        }

        /// <summary>
        /// Returns a string representation of the unit.
        /// </summary>
        /// <remarks>
        /// The format string can be either 'N' (Unit Name) or 'S' (Unit Symbol).
        /// </remarks>
        public string ToString( string format, IFormatProvider formatProvider )
        {
            if( format == null ) format = MeasureUnit.UnitSymbolFormatCode;

            if( formatProvider != null )
            {
                // ICustomFormatter formatter = formatProvider.GetFormat(GetType()) as ICustomFormatter;
                if( formatProvider.GetFormat( GetType() ) is ICustomFormatter formatter )
                {
                    return formatter.Format( format, this, formatProvider );
                }
            }

            switch( format )
            {
                case MeasureUnit.UnitNameFormatCode:
                    return this.Name;
                case MeasureUnit.UnitSymbolFormatCode:
                default:
                    return this.Symbol;
            }
        }

#endregion Representation (To String)
    }
}
