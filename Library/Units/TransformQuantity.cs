using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.UnitsOfMeasure
{

    /// <summary>
    /// Delegate representing a unidirectional unit transformation function.
    /// </summary>
    /// <param name="sourceQuantity">The quantity to be transformed.</param>
    /// <returns>The resulting quantity.</returns>
    public delegate Quantity TransformationFunction( Quantity sourceQuantity );

    public struct TransformQuantity : IEquatable<TransformQuantity>, IComparable<TransformQuantity>
    {

#region .Construction

        /// <summary> Initializes a new <see cref="TransformQuantity"/>. </summary>
        ///
        /// <param name="value">        Quantity value. </param>
        /// <param name="unit">         Unit of measure. Can be null (<see cref="MeasureUnit.None"/> is
        ///                             used). </param>
        /// <param name="baseUnit">     The base unit. </param>
        /// <param name="toBaseQuantity">   the transformation function to a base unit. </param>
        /// <param name="fromBaseQuantity"> the transformation function from a base unit. </param>
        public TransformQuantity( double value, MeasureUnit unit, MeasureUnit baseUnit , TransformationFunction toBaseQuantity, TransformationFunction fromBaseQuantity )
        {
            if( toBaseQuantity == null ) throw new ArgumentNullException( nameof( toBaseQuantity ) );
            this.Quantity = new Quantity( value, unit );
            this.BaseQuantity = new Quantity( toBaseQuantity(this.Quantity).Value, baseUnit);
            this.ToBaseQuantity = toBaseQuantity;
            this.FromBaseQuantity = fromBaseQuantity ?? throw new ArgumentNullException( nameof( fromBaseQuantity ) );
        }

        /// <summary>   Initializes a new <see cref="TransformQuantity"/>. </summary>
        ///
        /// <param name="unit">             Unit of measure. Can be null (<see cref="MeasureUnit.None"/> is used). </param>
        /// <param name="baseValue">        The base value. </param>
        /// <param name="baseUnit">         The base unit. </param>
        /// <param name="toBaseQuantity">   the transformation function to a base unit. </param>
        /// <param name="fromBaseQuantity"> the transformation function from a base unit. </param>
        public TransformQuantity( MeasureUnit unit, double baseValue,  MeasureUnit baseUnit, TransformationFunction toBaseQuantity, TransformationFunction fromBaseQuantity )
        {
            this.BaseQuantity = new Quantity( baseValue, baseUnit );
            this.Quantity = new Quantity( this.BaseQuantity.Value , unit );
            this.ToBaseQuantity = toBaseQuantity ?? throw new ArgumentNullException( nameof( toBaseQuantity ) );
            this.FromBaseQuantity = fromBaseQuantity ?? throw new ArgumentNullException( nameof( fromBaseQuantity ) );
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="unit">             Unit of measure. Can be null (<see cref="MeasureUnit.None"/> is used). </param>
        /// <param name="baseQuantity">     The base quantity. </param>
        /// <param name="toBaseQuantity">   the transformation function to a base unit. </param>
        /// <param name="fromBaseQuantity"> the transformation function from a base unit. </param>
        public TransformQuantity( MeasureUnit unit, Quantity baseQuantity , TransformationFunction toBaseQuantity, TransformationFunction fromBaseQuantity )
        {
            if( baseQuantity == null ) throw new ArgumentNullException( nameof( baseQuantity ) );
            this.BaseQuantity = new Quantity( baseQuantity.Value, baseQuantity.Unit );
            this.Quantity = new Quantity( this.BaseQuantity.Value, unit );
            this.ToBaseQuantity = toBaseQuantity ?? throw new ArgumentNullException( nameof( toBaseQuantity ) );
            this.FromBaseQuantity = fromBaseQuantity ?? throw new ArgumentNullException( nameof( fromBaseQuantity ) );
        }

        /// <summary>   Gets the quantity. </summary>
        ///
        /// <value> The quantity. </value>
        public Quantity Quantity { get; }

        /// <summary>   Gets the quantity. </summary>
        ///
        /// <value> The base quantity. </value>
        public Quantity BaseQuantity { get; }

        /// <summary> Gets the <see cref="TransformationFunction"/>to base quantity. </summary>
        ///
        /// <value> The <see cref="TransformationFunction"/>to base quantity. </value>
        public TransformationFunction ToBaseQuantity { get; }

        /// <summary> Gets the <see cref="TransformationFunction"/>from base quantity. </summary>
        ///
        /// <value> The <see cref="TransformationFunction"/>from base quantity. </value>
        public TransformationFunction FromBaseQuantity  { get; }

#endregion Construction

#region .Transformation

        /// <summary>
        /// Checks whether this quantity can be transformed into a quantity with a different <see cref="MeasureUnit"/>.
        /// </summary>
        /// <param name="targetUnit">The target unit.</param>
        /// <returns>True if this quantity can be expressed in the target unit, false otherwise.</returns>
        public bool CanTransformTo( MeasureUnit targetUnit )
        {
            return BaseQuantity.CanConvertTo( targetUnit );
        }

        /// <summary>
        /// Transforms this quantity to another <see cref="MeasureUnit"/>.
        /// Must be called only if <see cref="CanTransformTo(MeasureUnit)"/> returned true otherwise an
        /// <see cref="ArgumentException"/>
        /// is thrown.
        /// </summary>
        ///
        /// <param name="targetUnit">   The target unit. </param>
        ///
        /// <returns>   The quantity expressed with the target unit. </returns>
        ///
        public Quantity TransformTo( MeasureUnit targetUnit )
        {
            return Quantity.Convert( BaseQuantity, targetUnit );
        }

#endregion Transformation

#region .Operations

        /// <summary>
        /// Multiplies this quantity with another one.
        /// This is always possible: the resulting quantity's unit will hold the combined unit.
        /// </summary>
        /// <param name="multiplyingQuantity">Quantity to multiply.</param>
        /// <returns>The resulting quantity.</returns>
        public TransformQuantity Multiply( Quantity multiplyingQuantity )
        {
            return new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Multiply( this.BaseQuantity, multiplyingQuantity ), this.ToBaseQuantity, this.FromBaseQuantity );
        }

        /// <summary>
        /// Multiplies this quantity with another one. This is always possible: the resulting quantity's
        /// unit will hold the combined unit.
        /// </summary>
        ///
        /// <param name="value">    The value. </param>
        ///
        /// <returns>   The resulting quantity. </returns>

        public TransformQuantity Multiply( double value )
        {
            return new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Multiply( this.BaseQuantity, value ), this.ToBaseQuantity, this.FromBaseQuantity );
        }

        /// <summary>
        /// Divides this quantity with another one.
        /// This is always possible: the resulting quantity's unit will hold the combined unit.
        /// </summary>
        /// <param name="dividingQuantity">Quantity divisor.</param>
        /// <returns>The resulting quantity.</returns>
        public TransformQuantity DivideBy( Quantity dividingQuantity ) => new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Divide( this.BaseQuantity, dividingQuantity ), this.ToBaseQuantity, this.FromBaseQuantity );

        /// <summary>
        /// Divides this quantity with another one. This is always possible: the resulting quantity's
        /// unit will hold the combined unit.
        /// </summary>
        ///
        /// <param name="value">    The value. </param>
        ///
        /// <returns>   The resulting quantity. </returns>
        public TransformQuantity DivideBy( double value) => new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Divide( this.BaseQuantity, value ), this.ToBaseQuantity, this.FromBaseQuantity );

        /// <summary>   Divides the <paramref name="value"/> by this quantity. </summary>
        ///
        /// <param name="value">    The value. </param>
        ///
        /// <returns>   A TransformQuantity. </returns>
        public TransformQuantity Divides( double value ) => new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Divide( value , this.BaseQuantity ), this.ToBaseQuantity, this.FromBaseQuantity );

        /// <summary>
        /// Inverts this quantity.
        /// The result' value is 1/<see cref="Quantity.Value"/> and its <see cref="Quantity.Unit"/> is <see cref="MeasureUnit.Invert"/>.
        /// </summary>
        /// <returns>The inverse quantity.</returns>
        public TransformQuantity Invert() => new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Invert( this.BaseQuantity ), this.ToBaseQuantity, this.FromBaseQuantity );

        /// <summary>
        /// Elevates this quantity to a given power.
        /// </summary>
        /// <param name="exp">The exponent.</param>
        /// <returns>The resulting quantity.</returns>
        public TransformQuantity Power( int exp ) => new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Power( this.BaseQuantity , exp ), this.ToBaseQuantity, this.FromBaseQuantity );

        /// <summary>
        /// Checks whether another quantity can be added to this one.
        /// The added quantity must be convertible (see <see cref="TransformQuantity.CanTransformTo(MeasureUnit)"/>) into this <see cref="Quantity.Unit"/>.
        /// </summary>
        /// <param name="addedQuantity">The quantity that may be added to this quantity.</param>
        /// <returns>True if <see cref="Add(Quantity)"/> can be called.</returns>
        public bool CanAdd( Quantity addedQuantity ) => addedQuantity.CanConvertTo( this.BaseQuantity.Unit );

        /// <summary>
        /// Adds a given quantity to this one, returning a quantity with this <see cref="Quantity.Unit"/>.
        /// The quantity to add must be convertible into this <see cref="Quantity.Unit"/> (<see cref="TransformQuantity.CanAdd"/> must be true)
        /// otherwise an <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="addedQuantity">The quantity to add.</param>
        /// <returns>A quantity with this <see cref="Quantity.Unit"/>.</returns>
        public TransformQuantity Add( Quantity addedQuantity ) => new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Add( this.BaseQuantity, addedQuantity ), this.ToBaseQuantity, this.FromBaseQuantity );

        /// <summary>
        /// Negates this quantity: it is the negated <see cref="Quantity.Value"/> with the same <see cref="Quantity.Unit"/>.
        /// </summary>
        /// <returns>The negated quantity.</returns>
        public TransformQuantity Negate()
        {
            return new TransformQuantity( this.Quantity.Unit, CK.UnitsOfMeasure.Quantity.Negate( this.BaseQuantity ), this.ToBaseQuantity, this.FromBaseQuantity );
        }

        #endregion . Operations

#region .Operators

#pragma warning disable 1591
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates" )]
        public static TransformQuantity operator /( TransformQuantity left, Quantity right ) => left.DivideBy( right );
        public static TransformQuantity operator *( TransformQuantity left, Quantity right ) => left.Multiply( right );
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates" )]
        public static TransformQuantity operator ^( TransformQuantity quantity, int exp ) => quantity.Power( exp );
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates" )]
        public static TransformQuantity operator /( TransformQuantity quantity, double denominator ) => quantity.DivideBy( denominator );
        public static TransformQuantity operator *( TransformQuantity quantity, double factor ) => quantity.Multiply(factor);
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates" )]
        public static TransformQuantity operator /( double numerator, TransformQuantity quantity ) => quantity.Divides( numerator );
        public static TransformQuantity operator *( double factor, TransformQuantity quantity ) => quantity.Multiply( factor );
        public static TransformQuantity operator +( TransformQuantity left, Quantity right ) => left.Add( right );
        public static TransformQuantity operator -( TransformQuantity quantity ) => quantity.Negate();
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Usage", "CA2225:OperatorOverloadsHaveNamedAlternates" )]
        public static TransformQuantity operator -( TransformQuantity left, Quantity right ) => left.Add( right.Negate() );
        public static bool operator ==( TransformQuantity left, Quantity right ) => left.Equals( right );
        public static bool operator !=( TransformQuantity left, Quantity right ) => !left.Equals( right );
        public static bool operator >( TransformQuantity left, TransformQuantity right ) => left.CompareTo( right ) > 0;
        public static bool operator <( TransformQuantity left, TransformQuantity right ) => left.CompareTo( right ) < 0;
        public static bool operator >=( TransformQuantity left, TransformQuantity right ) => left.CompareTo( right ) >= 0;
        public static bool operator <=( TransformQuantity left, TransformQuantity right ) => left.CompareTo( right ) <= 0;
#pragma warning restore 1591

#endregion Operators

#region .Equality

        /// <summary>
        /// Overridden to support Unit aware equality. See <see cref="Equals(TransformQuantity)"/>.
        /// </summary>
        /// <param name="obj">The object to test.</param>
        /// <returns>True if this quantity is the same as the one, false otherwise.</returns>
        public override bool Equals( object obj ) => obj is TransformQuantity q && this.Equals( q );

        /// <summary>
        /// Overridden to support Unit aware equality.
        /// </summary>
        /// <returns>The hash code to use for this quantity.</returns>
        public override int GetHashCode() => Quantity.GetHashCode(this.BaseQuantity);

        /// <summary>
        /// Checks if this quantity is equal to another one: its <see cref="Quantity.ToNormalizedString(Quantity)"/>
        /// is the same as the other quantity (and they belong to the same <see cref="MeasureContext"/>).
        /// </summary>
        /// <param name="other">The quantity that may be equal to this.</param>
        /// <returns>True if this quantity is the same as the other one, false otherwise.</returns>
        public bool Equals( TransformQuantity other ) => Quantity.Equals(this.BaseQuantity, other.BaseQuantity);

        /// <summary>
        /// Compares this quantity to another one.
        /// </summary>
        /// <param name="other">The other quantity to compare.</param>
        /// <returns></returns>
        public int CompareTo( TransformQuantity other ) => Quantity.CompareTo( this.BaseQuantity, other.BaseQuantity );

        #endregion Equality

#region .Transform Quantities

        /// <summary>   The standard units. </summary>
        private static StandardUnitsContext _standardUnitsContext = StandardUnitsContext.Default;

        /// <summary>   Centigrade <see cref="TransformQuantity"/> </summary>
        ///
        /// <param name="value">    Quantity value. </param>
        ///
        /// <returns>   A TransformQuantity. </returns>

        public static TransformQuantity CentigradeQuantity(double value )
        {
            {
                return new TransformQuantity(value, _standardUnitsContext.Centigrade, _standardUnitsContext.Kelvin,
                        delegate ( Quantity quantity ) {return new Quantity( quantity.Value + 273.15, _standardUnitsContext.Kelvin);},
                        delegate ( Quantity quantity ) { return new Quantity( quantity.Value - 273.15, _standardUnitsContext.Centigrade ); });
            }
        }

        /// <summary>   Fahrenheit <see cref="TransformQuantity"/> </summary>
        ///
        /// <param name="value">    Quantity value. </param>
        ///
        /// <returns>   A TransformQuantity. </returns>

        public static TransformQuantity FahrenheitQuantity( double value )
        {
            {
                return new TransformQuantity( value, _standardUnitsContext.Centigrade, _standardUnitsContext.Kelvin,
                        delegate ( Quantity quantity ) { return new Quantity( 0.8 *( quantity.Value + 273.15) + 32 , _standardUnitsContext.Kelvin ); },
                        delegate ( Quantity quantity ) { return new Quantity( (quantity.Value - 273.15 - 32) / 1.8, _standardUnitsContext.Fahrenheit ); } );
            }
        }

        /// <summary>   Bel voltage quantity. </summary>
        ///
        /// <param name="value">    Quantity value. </param>
        ///
        /// <returns>   A TransformQuantity. </returns>

        public static TransformQuantity BelVoltageQuantity( double value )
        {
            {
                return new TransformQuantity( value, _standardUnitsContext.Bel, _standardUnitsContext.Volt,
                        delegate ( Quantity quantity ) { return new Quantity( Math.Pow(10, quantity.Value / 2) , _standardUnitsContext.Volt ); },
                        delegate ( Quantity quantity ) { return new Quantity( 2 * Math.Log10( quantity.Value ), _standardUnitsContext.Bel ); } );
            }
        }

        /// <summary>   Bel watt quantity. </summary>
        ///
        /// <param name="value">    Quantity value. </param>
        ///
        /// <returns>   A TransformQuantity. </returns>

        public static TransformQuantity BelWattQuantity( double value )
        {
            {
                return new TransformQuantity( value, _standardUnitsContext.Bel, _standardUnitsContext.Watt,
                        delegate ( Quantity quantity ) { return new Quantity( Math.Pow( 10, quantity.Value ), _standardUnitsContext.Watt ); },
                        delegate ( Quantity quantity ) { return new Quantity( Math.Log10( quantity.Value ), _standardUnitsContext.Bel ); } );
            }
        }

        /// <summary>   Neper bel quantity. </summary>
        ///
        /// <param name="value">    Quantity value. </param>
        ///
        /// <returns>   A TransformQuantity. </returns>

        public static TransformQuantity NeperBelQuantity( double value )
        {
            {
                return new TransformQuantity( value, _standardUnitsContext.Neper, _standardUnitsContext.Bel,
                        delegate ( Quantity quantity ) { return new Quantity( 0.5 * Math.Log( quantity.Value ), _standardUnitsContext.Bel ); },
                        delegate ( Quantity quantity ) { return new Quantity( 2 * Math.Log10( Math.E ) * quantity.Value, _standardUnitsContext.Neper ); } );
            }
        }

        /// <summary>   Neper unit quantity. </summary>
        ///
        /// <param name="value">    Quantity value. </param>
        ///
        /// <returns>   A TransformQuantity. </returns>

        public static TransformQuantity NeperUnitQuantity( double value )
        {
            {
                return new TransformQuantity( value, _standardUnitsContext.Neper, _standardUnitsContext.Volt,
                        delegate ( Quantity quantity ) { return new Quantity( Math.Pow( Math.E , quantity.Value ), _standardUnitsContext.Unit ); },
                        delegate ( Quantity quantity ) { return new Quantity( Math.Log( quantity.Value ), _standardUnitsContext.Neper ); } );
            }
        }

        #endregion

    }
}
