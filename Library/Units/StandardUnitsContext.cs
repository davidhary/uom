using System;

namespace CK.UnitsOfMeasure
{
    /// <summary> Specialized <see cref="MeasureContext"/> that defines standard and commonly used units. </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/21/2019" by="David" revision="">   Created. </history>
    public sealed partial class StandardUnitsContext : MeasureContext
    {

        // static holder for instance, need to use lambda to construct since constructor private
        private static readonly Lazy<StandardUnitsContext> _default = new Lazy<StandardUnitsContext>(() => new StandardUnitsContext( String.Empty, true ) );

        // accessor for the default context singleton
        public static StandardUnitsContext Default
        {
           get
           {
               return _default.Value;
           }
        }

        /// <summary>
        /// Initializes a new <see cref="StandardUnitsContext"/> with a name that should uniquely identify this
        /// context.
        /// </summary>
        ///
        /// <param name="name">         Name of this context. Must not be null or empty: the empty name
        ///                             is reserved for <see cref="Default"/>. </param>
        /// <param name="isDefault">    True if is default, false if not. </param>

        public StandardUnitsContext( string name , bool isDefault ) : base( name, isDefault )
        {
            // fundamental units
            this.Unit = this.DefineFundamental( "#", "Unit", AutoStandardPrefix.Metric );
            this.Meter = this.DefineFundamental( "m", "Meter", AutoStandardPrefix.Metric );
            this.Gram = this.DefineFundamental( "g", "Gram", AutoStandardPrefix.Metric, MeasureStandardPrefix.Kilo );
            this.Kilogram = (PrefixedMeasureUnit)this[$"{MeasureStandardPrefix.Kilo}g"];
            this.Second = this.DefineFundamental( "s", "Second", AutoStandardPrefix.Metric );
            this.Ampere = this.DefineFundamental( "A", "Ampere", AutoStandardPrefix.Metric );
            this.Kelvin = this.DefineFundamental( "K", "Kelvin", AutoStandardPrefix.Metric );
            this.Mole = this.DefineFundamental( "mol", "Mole", AutoStandardPrefix.Metric );
            this.Candela = this.DefineFundamental( "cd", "Candela", AutoStandardPrefix.Metric );
            this.Bit = this.DefineFundamental( "b", "Bit", AutoStandardPrefix.Both );

            this.Centigrade = this.DefineFundamental( $"{UnitSymbols.Degrees}C", "Centigrade", AutoStandardPrefix.None );
            this.Fahrenheit = this.DefineFundamental( $"{UnitSymbols.Degrees}F", "Fahrenheit", AutoStandardPrefix.None );
            this.Neper = this.DefineFundamental( $"nP", "Neper", AutoStandardPrefix.None );
            this.Bel = this.DefineFundamental( $"Bel", "Bel", AutoStandardPrefix.Metric );

            _kg = Kilogram;
            _m = Meter;
            _s = Second;
            _a = Ampere;

            // atomic units 
            this.Decimeter = MeasureStandardPrefix.Deci[this.Meter];
            this.Kilometer = MeasureStandardPrefix.Kilo[this.Meter];
            this.Micron = MeasureStandardPrefix.Micro[this.Meter];
            this.Micrometer = MeasureStandardPrefix.Micro[this.Meter];

            // alias units
            this.Byte = this.DefineAlias( "B", "Byte", new FullFactor( new ExpFactor( 3, 0 ) ), this.Bit, AutoStandardPrefix.Both );

            this.Percent = this.DefineAlias( "%", "Percent", new ExpFactor( 0, -2 ), MeasureUnit.None );
            this.Permille = this.DefineAlias( $"{UnitSymbols.PerThousand}", "Permille", new ExpFactor( 0, -3 ), MeasureUnit.None );
            this.Permyriad = this.DefineAlias( $"{UnitSymbols.PerTenThousand}", "Permyriad", new ExpFactor( 0, -4 ), MeasureUnit.None );
            this.Ppm = this.DefineAlias( $"{UnitSymbols.Ppm}", "PPM", new ExpFactor( 0, -6 ), MeasureUnit.None );

            this.Inch = this.DefineAlias( "in", "Inch", 0.0254, Meter );
            this.Foot = this.DefineAlias( "ft", "Foot", 12, Inch );
            this.Yard = this.DefineAlias( "yd", "Yard", 36, Inch );
            this.Mile = this.DefineAlias( "mi", "Mile", 5280, Foot );
            this.NauticalMile = this.DefineAlias( "nmi", "Nautical Mile", 1852, Meter );
            this.Lightyear = this.DefineAlias( "ly", "Lightyear", 9460730472580800.0, Meter );

            this.Gallon = this.DefineAlias( "gal", "Gallon", 231, Inch ^ 3 );
            this.MCF = this.DefineAlias( "MCF", "MCF", 1000, Foot );
            this.MMCF = this.DefineAlias( "MMCF", "MMCF", 1000000, Foot );

            this.Minute = this.DefineAlias( "min", "Minute", 60, Second );
            this.Hour = this.DefineAlias( "h", "Hour", 3600, Second );
            this.Day = this.DefineAlias( "d", "Day", 24, Hour );

            this.Knot = this.DefineAlias( "kn", "Knot", 1.852, Kilometer / Hour );

            this.Newton = this.DefineAlias( "N", "Newton", FullFactor.Neutral, _kg * _m * (_s ^ -2), AutoStandardPrefix.Metric );
            this.Dyne = this.DefineAlias( "dyn", "Dyne", new ExpFactor( 0, -5 ), Newton, AutoStandardPrefix.Metric );
            this.Gravity = this.DefineAlias( $"g{UnitSymbols.Subscript(0)}", "Gravity", 9.80665, _m * (_s ^ -2), AutoStandardPrefix.Metric );
            this.Kilopond = this.DefineAlias( "kp", "Kilopond", 9.80665, Newton );
            this.KilogramForce = this.DefineAlias( "kgf", "Kilogram force", 9.80665, Newton );
            this.Pound = this.DefineAlias( "lbf", "Pound", 4.4482216, Newton );

            this.Joule  = this.DefineAlias( "J", "Joule", FullFactor.Neutral, _kg * (_m ^ 2) * (_s ^ -2), AutoStandardPrefix.Metric );
            this.Watt = this.DefineAlias( "W", "Watt", FullFactor.Neutral, this.Joule / _s, AutoStandardPrefix.Metric );
            this.Calorie = this.DefineAlias( "cal", "Calorie", 4.1868, Joule, AutoStandardPrefix.Metric );
            this.Horsepower = this.DefineAlias( "hp", "Horsepower", 735.49875, Watt );

            this.Coulomb = this.DefineAlias( "C", "Coulomb", FullFactor.Neutral, _a * _s, AutoStandardPrefix.Metric );
            this.Volt = this.DefineAlias( "V", "Volt", FullFactor.Neutral, this.Watt / _a, AutoStandardPrefix.Metric );
            this.Ohm = this.DefineAlias( $"{UnitSymbols.Omega}", "Ohm", FullFactor.Neutral, this.Volt / _a, AutoStandardPrefix.Metric );
            this.Mho = this.DefineAlias( $"{UnitSymbols.OmegaInverted}", "Mho", FullFactor.Neutral, _a / this.Volt, AutoStandardPrefix.Metric );
            this.Farad = this.DefineAlias( $"F", "Farad", FullFactor.Neutral, this.Coulomb / this.Volt, AutoStandardPrefix.Metric );
            this.Henry = this.DefineAlias( $"H", "Henry", FullFactor.Neutral, this.Ohm * _s, AutoStandardPrefix.Metric );
            this.Seebeck = this.DefineAlias( $"V/{UnitSymbols.Degrees}", "Seebeck", FullFactor.Neutral, this.Volt / this.Kelvin, AutoStandardPrefix.Metric );
            this.OhmPerSquare = this.DefineAlias( $"{UnitSymbols.Omega}/{UnitSymbols.WhiteSquare}", "Ohm/sq", FullFactor.Neutral, this.Ohm * _m, AutoStandardPrefix.Metric );

            this.Pascal = this.DefineAlias( $"Pa", "Pascal", FullFactor.Neutral, this.Newton * (_m ^ -2), AutoStandardPrefix.Metric );
            this.Bar = this.DefineAlias( $"bar", "Bar", 100000, this.Pascal, AutoStandardPrefix.Metric );
            this.Atmosphere = this.DefineAlias( $"atm", "Atmosphere", 101325, this.Pascal, AutoStandardPrefix.Metric );
            this.PSI = this.DefineAlias( $"psi", "PSI", 6894.7, this.Pascal, AutoStandardPrefix.Metric );
            this.WaterColumn = this.DefineAlias( $"wc", "Water Column", 249.088908333, Pascal, AutoStandardPrefix.None );

            this.Hertz = this.DefineAlias( $"Hz", "Hertz", FullFactor.Neutral, this.Unit * (_s ^ -1), AutoStandardPrefix.Metric );
            this.RPM = this.DefineAlias( $"RPM", "RPM", FullFactor.Neutral, this.Unit * (this.Minute ^ -1), AutoStandardPrefix.Metric );

            // measure units
            this.OhmMeter = this.Ohm * this.Meter;
            this.CubicFootPerHour = (this.Foot ^ 3) / this.Hour;
            this.MCFPerDay = this.MCF / this.Day;
            this.MMCFPerDay = this.MMCF / this.Day;
            this.KilometerPerHour = this.Kilometer / this.Hour;
            this.Liter = this.Decimeter ^ 3;
            this.SquareMeter =this.Meter ^ 2;
            this.CubicMeter = this.Meter ^ 3;
        }

#region . Fundamental Units .

        /// <summary>   Dimensionless unit. Used to count items. Associated symbol is "#". </summary>
        ///
        /// <value> The unit. </value>
        public FundamentalMeasureUnit Unit { get; } 

        /// <summary>
        /// The metre is the length of the path traveled by light in vacuum during a time interval of
        /// 1/299792458 of a second. This is the SI base unit of measure of distance. Associated symbol
        /// is "m".
        /// </summary>
        ///
        /// <value> The meter. </value>
        public FundamentalMeasureUnit Meter { get; } 

        /// <summary>
        /// The gram is our fundamental unit of mass (see <see cref="Kilogram"/>). Associated symbol is
        /// "g".
        /// </summary>
        ///
        /// <value> The gram. </value>
        public FundamentalMeasureUnit Gram { get; } 

        /// <summary>
        /// The kilogram is the unit of mass; it is equal to the mass of the international prototype of
        /// the kilogram. This is the only SI base unit that includes a prefix. To avoid coping with this
        /// exception in the code, we define it as a <see cref="PrefixedMeasureUnit"/> based on the gram
        /// (<see cref="MeasureStandardPrefix.Kilo"/>
        /// of <see cref="Gram"/>) . Associated symbol is "kg".
        /// </summary>
        ///
        /// <value> The kilogram. </value>
        public PrefixedMeasureUnit Kilogram { get; } 

        /// <summary>
        /// The second is the duration of 9192631770 periods of the radiation corresponding to the
        /// transition between the two hyperfine levels of the ground state of the cesium 133 atom. This
        /// is the SI base unit of measure of time. Associated symbol is "s".
        /// </summary>
        ///
        /// <value> The second. </value>
        public FundamentalMeasureUnit Second { get ; } 

        /// <summary>
        /// The ampere is that constant current which, if maintained in two straight parallel conductors
        /// of infinite length, of negligible circular cross-section, and placed 1 metre apart in vacuum,
        /// would produce between these conductors a force equal to 2×10−7 newton per metre of length.
        /// This is the SI base unit of measure of electric current. Associated symbol is "A".
        /// </summary>
        ///
        /// <value> The ampere. </value>
        public FundamentalMeasureUnit Ampere { get; } 

        /// <summary>
        /// The kelvin, unit of thermodynamic temperature, is the fraction 1 / 273.16 of the
        /// thermodynamic temperature of the triple point of water. This is the SI base unit of measure
        /// of thermodynamic temperature. Associated symbol is "K".
        /// </summary>
        ///
        /// <value> The kelvin. </value>
        public FundamentalMeasureUnit Kelvin { get; } 

        /// <summary>
        /// The mole is the amount of substance of a system which contains as many elementary entities as
        /// there are atoms in 0.012 kilogram of carbon 12; its symbol is 'mol'. This is the SI base unit
        /// of measure of an amount of substance. Associated symbol is "mol".
        /// </summary>
        ///
        /// <value> The mole. </value>
        public FundamentalMeasureUnit Mole { get; }

        /// <summary>
        /// The candela is the luminous intensity, in a given direction, of a source that emits
        /// monochromatic radiation of frequency 540×1012 hertz and that has a radiant intensity in that
        /// direction of 1/683 watt per steradian. This is the SI base unit of measure of luminous
        /// intensity. Associated symbol is "cd".
        /// </summary>
        ///
        /// <value> The candela. </value>
        public FundamentalMeasureUnit Candela { get; } 

        /// <summary>
        /// A bit is defined as the information entropy of a binary random variable that is 0 or 1 with
        /// equal probability. Associated symbol is "b" (recommended by the IEEE 1541-2002 and IEEE Std
        /// 260.1-2004 standards).
        /// </summary>
        ///
        /// <value> The bit. </value>
        public FundamentalMeasureUnit Bit { get; } 

        /// <summary>   Gets the centigrade. </summary>
        ///
        /// <value> The centigrade. </value>
        public FundamentalMeasureUnit Centigrade { get; } 

        /// <summary>   Gets the Fahrenheit. </summary>
        ///
        /// <value> The Fahrenheit unit. </value>
        public FundamentalMeasureUnit Fahrenheit { get; } 

        /// <summary>
        /// The Neper (unit symbol Np) is a logarithmic unit for ratios of measurements of physical field
        /// and power quantities, such as gain and loss of electronic signals. The unit's name is derived
        /// from the name of John Napier, the inventor of logarithms.
        /// </summary>
        ///
        /// <value> The neper. </value>
        public FundamentalMeasureUnit Neper { get; }

        /// <summary>
        /// The Bel (unit symbol Bel) is a base-10 logarithmic unit for ratios of measurements of physical field
        /// and power quantities, such as gain and loss of electronic signals. The unit's name is derived
        /// from the name of Alexander Graham Bell.
        /// </summary>
        ///
        /// <value> The bel. </value>
        public FundamentalMeasureUnit Bel { get; } 

        /// <summary>   The kilograms. </summary>
        private readonly PrefixedMeasureUnit _kg;
        /// <summary>   The m. </summary>
        private readonly FundamentalMeasureUnit _m;
        /// <summary>   The s. </summary>
        private readonly FundamentalMeasureUnit _s;
        /// <summary>   a. </summary>
        private readonly FundamentalMeasureUnit _a;

#endregion Fundamental Units

#region . Atomic Units .

        /// <summary>   Gets the decimeter. </summary>
        ///
        /// <value> The decimeter. </value>
        public AtomicMeasureUnit Decimeter { get; } 

        /// <summary>   Gets the kilometer. </summary>
        ///
        /// <value> The kilometer. </value>
        public AtomicMeasureUnit Kilometer { get; } 

        /// <summary>
        /// Micron is a length measurement unit. micron is an unofficial name for micrometer (μm).
        /// </summary>
        ///
        /// <value> The micron. </value>
        public AtomicMeasureUnit Micron { get; }

        /// <summary>   Micrometer (μm). </summary>
        ///
        /// <value> The micro meter. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "Micrometer" )]
        public AtomicMeasureUnit Micrometer { get; } 

        #endregion Atomic Units

#region . Alias Units .

        /// <summary>
        /// A byte is now standardized as eight bits, as documented in ISO/IEC 2382-1:1993. The
        /// international standard IEC 80000-13 codified this common meaning. Associated symbol is "B"
        /// and it is an alias with a <see cref="ExpFactor"/> of 2^3 on <see cref="Bit"/>.
        /// </summary>
        ///
        /// <value> The byte. </value>
        public  AliasMeasureUnit Byte { get; } 

        /// <summary>   Gets the percent; parts per one hundred. </summary>
        ///
        /// <value> The percent; parts per one hundred. </value>
        public AliasMeasureUnit Percent { get; }

        /// <summary>   Gets the permille; parts per one thousand. </summary>
        ///
        /// <value> The permille; parts per one thousand. </value>
        public AliasMeasureUnit Permille { get; } 

        /// <summary>   Gets the permyriad; parts per one ten thousand. </summary>
        ///
        /// <value> The permyriad; parts per ten thousand. </value>
        public AliasMeasureUnit Permyriad{ get; }

        /// <summary>   Gets the ppm; parts per million. </summary>
        ///
        /// <value> The ppm; parts per million. </value>
        public AliasMeasureUnit Ppm { get; } 

        /// <summary>   Gets the inch. </summary>
        ///
        /// <value> The inch. </value>
        public AliasMeasureUnit Inch { get; } 

        /// <summary>   Gets the foot. </summary>
        ///
        /// <value> The foot. </value>
        public AliasMeasureUnit Foot { get; } 

        /// <summary>   Gets the yard. </summary>
        ///
        /// <value> The yard. </value>
        public AliasMeasureUnit Yard { get; } 

        /// <summary>   Gets the mile. </summary>
        ///
        /// <value> The mile. </value>
        public AliasMeasureUnit Mile { get; } 

        /// <summary>   Gets the nautical mile. </summary>
        ///
        /// <value> The nautical mile. </value>
        public AliasMeasureUnit NauticalMile { get; }

        /// <summary>   Gets the lightyear. </summary>
        ///
        /// <value> The lightyear. </value>
        public  AliasMeasureUnit Lightyear { get; }

        /// <summary>   Gets the gallon. </summary>
        ///
        /// <value> The gallon. </value>
        public AliasMeasureUnit Gallon { get; }

        /// <summary>   Gets the MCF. </summary>
        ///
        /// <value> The MCF. </value>
        public AliasMeasureUnit MCF { get; } 

        /// <summary>   Gets the MMCF. </summary>
        ///
        /// <value> The MMCF. </value>
        public AliasMeasureUnit MMCF { get; } 

        /// <summary>   Gets the minute. </summary>
        ///
        /// <value> The minute. </value>
        public AliasMeasureUnit Minute { get; }

        /// <summary>   Gets the hour. </summary>
        ///
        /// <value> The hour. </value>
        public AliasMeasureUnit Hour { get; } 

        /// <summary>   Gets the day. </summary>
        ///
        /// <value> The day. </value>
        public AliasMeasureUnit Day { get; }

        /// <summary>   Gets the knot. </summary>
        ///
        /// <value> The knot. </value>
        public AliasMeasureUnit Knot { get; }

        /// <summary>   Gets the newton. </summary>
        ///
        /// <value> The newton. </value>
        public AliasMeasureUnit Newton { get; }

        /// <summary>   Gets the dyne. </summary>
        ///
        /// <value> The dyne. </value>
        public AliasMeasureUnit Dyne { get; }

        /// <summary>
        /// Gets the gravity. A conventional value approximating the average magnitude of gravity on
        /// earth.
        /// </summary>
        ///
        /// <value> The gravity. </value>

        public AliasMeasureUnit Gravity { get; }

        /// <summary>
        /// Gets the kilopond (equals <see cref="KilogramForce"/> is a gravitation unit of force. It is
        /// equals to the magnitude of the force exerted on one kilogram of mass in gravitational field.
        /// The term Kilopond was declared obsolete and should no longer be used.
        /// </summary>
        ///
        /// <value> The kilopond (equals <see cref="KilogramForce"/>. </value>
        public AliasMeasureUnit Kilopond { get; }

        /// <summary>
        /// Gets the kilogram-force (equals <see cref="Kilopond"/>) is a gravitation unit of force. This
        /// using is a non-standard unit and is classified in the SI Metric System as a unit that is
        /// unacceptable for use with SI.
        /// </summary>
        ///
        /// <value> The kilogram force. </value>
        public AliasMeasureUnit KilogramForce { get; }

        /// <summary>   Gets the pound. </summary>
        ///
        /// <value> The pound. </value>
        public AliasMeasureUnit Pound { get; }

        /// <summary>
        /// Gets the joule. the SI unit of work or energy, equal to the work done by a force of one
        /// newton when its point of application moves one meter in the direction of action of the force,
        /// equivalent to one 3600th of a watt-hour.
        /// </summary>
        ///
        /// <value> The joule. </value>
        public AliasMeasureUnit Joule { get; }

        /// <summary>
        /// Gets the watt. the SI unit of power, equivalent to one joule per second, corresponding to the
        /// power in an electric circuit in which the potential difference is one volt and the current
        /// one ampere.
        /// </summary>
        ///
        /// <value> The watt. </value>
        public AliasMeasureUnit Watt { get; }

        /// <summary>
        /// Gets the calorie. The small calorie or gram calorie (usually denoted cal) is the amount of
        /// heat energy needed to raise the temperature of one gram of water by one degree Celsius (or
        /// one kelvin). The large calorie, food calorie, or kilo-calorie (Cal or kcal) is the amount of
        /// heat needed to cause the same increase on one kilogram of water. Now usually defined as 4.1868 Joules
        /// </summary>
        ///
        /// <value> The calorie. </value>
        public AliasMeasureUnit Calorie { get; }

        /// <summary>
        /// Gets the metric horsepower. Horsepower (hp) is a unit of measurement of power, or the rate at which
        /// work is done. There are many different standards and types of horsepower. Two common
        /// definitions being used today are the mechanical horsepower (or imperial horsepower), which is
        /// about 745.7 watts, and the metric horsepower, which is approximately 735.5 watts.
        /// </summary>
        ///
        /// <value> The horsepower. </value>
        public AliasMeasureUnit Horsepower { get; }

        /// <summary>
        /// Gets the coulomb. The coulomb (symbol: C) is the International System of Units (SI) unit of
        /// electric charge. It is the charge (symbol: Q or q) transported by a constant current of one
        /// ampere in one second.
        /// </summary>
        ///
        /// <value> The coulomb. </value>

        public AliasMeasureUnit Coulomb { get; }

        /// <summary>
        /// Gets the volt. The volt (symbol: V) is the derived unit for electric potential, electric
        /// potential difference (voltage), and electromotive force.[1] It is named after the Italian
        /// physicist Alessandro Volta (1745–1827). One volt is defined as the difference in electric
        /// potential between two points of a conducting wire when an electric current of one ampere
        /// dissipates one watt of power between those points.[2] It is also equal to the potential
        /// difference between two parallel, infinite planes spaced 1 meter apart that create an electric
        /// field of 1 newton per coulomb. Additionally, it is the potential difference between two
        /// points that will impart one joule of energy per coulomb of charge that passes through it.
        /// </summary>
        ///
        /// <value> The volt. </value>
        public AliasMeasureUnit Volt { get; }

        /// <summary>
        /// Gets the ohm. is the SI derived unit of electrical resistance, named after German physicist
        /// Georg Simon Ohm. Although several empirically derived standard units for expressing
        /// electrical resistance were developed in connection with early telegraphy practice, the
        /// British Association for the Advancement of Science proposed a unit derived from existing
        /// units of mass, length and time and of a convenient size for practical work as early as 1861.
        /// The definition of the ohm was revised several times. Today, the definition of the ohm is
        /// expressed from the quantum Hall effect.
        /// </summary>
        ///
        /// <value> The ohm. </value>
        public AliasMeasureUnit Ohm { get; }

        /// <summary>
        /// Gets the mho or Siemens is the SI derived unit of electric conductance and admittance, also
        /// known as the mho (ohm spelled backwards); it is the reciprocal of resistance in ohms (Ω).
        /// </summary>
        ///
        /// <value> The mho. </value>
        public AliasMeasureUnit Mho { get; }

        /// <summary>
        /// Gets the farad. The farad (symbol: F) is the SI derived unit of electrical capacitance, the
        /// ability of a body to store an electrical charge. It is named after the English physicist
        /// Michael Faraday. One farad is defined as the capacitance across which, when charged with one
        /// coulomb, there is a potential difference of one volt.
        /// </summary>
        ///
        /// <value> The farad. </value>
        public AliasMeasureUnit Farad { get; }

        /// <summary>
        /// Gets the henry. The henry (symbol: H) is the SI derived unit of electrical inductance.[1] If
        /// a current of 1 ampere flowing through the coil produces flux linkage of 1 weber turn, the
        /// coil has a self inductance of 1 henry.‌ The unit is named after Joseph Henry (1797–1878). The
        /// inductance of an electric circuit is one henry when an electric current that is changing at
        /// one ampere per second results in an electromotive force of one volt across the inductor.
        /// </summary>
        ///
        /// <value> The henry. </value>
        public AliasMeasureUnit Henry { get; }

        /// <summary>
        /// Gets the seebeck. The SI unit of the Seebeck coefficient is volts per kelvin (V/K), although
        /// it is more often given in microvolts per kelvin (μV/K). The use of materials with a high
        /// Seebeck coefficient is one of many important factors for the efficient behavior of
        /// thermoelectric generators and thermoelectric coolers.
        /// </summary>
        ///
        /// <value> The seebeck. </value>
        public AliasMeasureUnit Seebeck { get; }

    /// <summary>
    /// Gets the ohm per square or sheet resistance. It is a measure of resistance of thin films that
    /// are nominally uniform in thickness. It is commonly used to characterize materials made by
    /// semiconductor doping, metal deposition, resistive paste printing, and glass coating. Examples
    /// of these processes are: doped semiconductor regions (e.g., silicon or polysilicon), and the
    /// resistors that are screen printed onto the substrates of thick-film hybrid microcircuits.
    /// 
    /// The utility of sheet resistance as opposed to resistance or resistivity is that it is
    /// directly measured using a four-terminal sensing measurement( also known as a four-point probe
    /// measurement) or indirectly by using a non-contact eddy current based testing device.Sheet
    /// resistance is invariable under scaling of the film contact and therefore can be used to
    /// compare the electrical properties of devices that are significantly different in size.
    /// </summary>
    ///
    /// <value> The ohm per square. </value>
    public AliasMeasureUnit OhmPerSquare { get; }

        /// <summary>
        /// Gets the pascal. The pascal (symbol: Pa) is the SI derived unit of pressure used to quantify
        /// internal pressure, stress, Young's modulus and ultimate tensile strength. It is defined as
        /// one newton per square metre.[1] It is named after the French polymath Blaise Pascal.
        /// </summary>
        ///
        /// <value> The pascal. </value>
        public AliasMeasureUnit Pascal { get; } 

        /// <summary>
        /// Gets the bar. The bar is a metric unit of pressure, but is not approved as part of the
        /// International System of Units (SI). It is defined as exactly equal to 100,000 Pa, which is
        /// slightly less than the current average atmospheric pressure on Earth at sea level.[1][2].
        /// </summary>
        ///
        /// <value> The bar. </value>

        public AliasMeasureUnit Bar { get; }

        /// <summary>
        /// Gets the atmosphere. The standard atmosphere (symbol: atm) is a unit of pressure defined as
        /// 101325 Pa (1.01325 bar). It is sometimes used as a reference or standard pressure. It is
        /// approximately equal to the atmospheric pressure at sea level.
        /// </summary>
        ///
        /// <value> The atmosphere. </value>
        public AliasMeasureUnit Atmosphere { get; }

        /// <summary>
        /// The pound per square inch or, more accurately, pound-force per square inch (symbol: lbf/in2;
        /// [1] abbreviation: psi) is a unit of pressure or of stress based on avoirdupois units. It is
        /// the pressure resulting from a force of one pound-force applied to an area of one square inch.
        /// In SI units, 1 psi is approximately equal to 6895 N/m2.
        /// </summary>
        ///
        /// <value> The psi. </value>
        public AliasMeasureUnit PSI { get; }

        /// <summary>
        /// Gets the inch of water column unit. Inches of water, inches of water gauge (iwg or in.w.g.),
        /// inches water column (inch wc or just wc), inAq, Aq, or inH2O is a non-SI unit for pressure.
        /// The units are conventionally used for measurement of certain pressure differentials such as
        /// small pressure differences across an orifice, or in a pipeline or shaft.[1] Inches of water
        /// can be converted to a pressure unit using the formula for pressure head.
        /// 
        /// It is defined as the pressure exerted by a column of water of 1 inch in height at defined
        /// conditions.At a temperature of 4 °C (39.2 °F) pure water has its highest density(1000 kg/m3).
        /// At that temperature and assuming the standard acceleration of gravity, 1 inAq is
        /// approximately 249.082 pascals.[.
        /// </summary>
        ///
        /// <value> The inch of water column unit. </value>
        public AliasMeasureUnit WaterColumn { get; }

        /// <summary>
        /// Gets the hertz. The hertz (symbol: Hz) is the derived unit of frequency in the International
        /// System of Units (SI) and is defined as one cycle per second.[1] It is named after Heinrich
        /// Rudolf Hertz, the first person to provide conclusive proof of the existence of
        /// electromagnetic waves.
        /// </summary>
        ///
        /// <value> The hertz. </value>

        public AliasMeasureUnit Hertz { get; }

        /// <summary>
        /// Gets the rpm. Revolutions per minute (abbreviated rpm, RPM, rev/min, r/min, or with the
        /// notation min−1) is the number of turns in one minute. It is a unit of rotational speed or the
        /// frequency of rotation around a fixed axis. According to the International System of Units
        /// (SI), rpm is not a unit. This is because the word revolution is a semantic annotation rather
        /// than a unit. The annotation is instead done as a subscript of the formula sign if needed.
        /// Because of the measured physical quantity, the formula sign has to be f for (rotational)
        /// frequency and ω or Ω for angular velocity. The corresponding basic SI derived unit is s−1 or
        /// Hz. When measuring angular speed, the unit radians per second is used.
        /// </summary>
        ///
        /// <value> The rpm. </value>
        public AliasMeasureUnit RPM { get; }

#endregion Alias Units

#region . Measure Units .

        /// <summary>
        /// Gets the ohm meter. The electrical resistivity is the electrical resistance per unit length
        /// and per unit of cross-sectional area at a specified temperature. The SI unit of electrical
        /// resistivity is the ohm⋅metre (Ω⋅m). It is commonly represented by the Greek letter ρ, rho.
        /// </summary>
        ///
        /// <value> The ohm meter. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "OhmMeter" )]
        public MeasureUnit OhmMeter { get; } 

        /// <summary>   Gets the cubic foot per hour. </summary>
        ///
        /// <value> The cubic foot per hour. </value>
        public MeasureUnit CubicFootPerHour { get; }

        /// <summary>   Gets the MCF per day. </summary>
        ///
        /// <value> The MCF per day. </value>
        public MeasureUnit MCFPerDay { get; } 

        /// <summary>   Gets the MMCF per day. </summary>
        ///
        /// <value> The MMCF per day. </value>
        public MeasureUnit MMCFPerDay { get; }

        /// <summary>   Gets the kilometer per hour. </summary>
        ///
        /// <value> The kilometer per hour. </value>
        public MeasureUnit KilometerPerHour { get; } 

        /// <summary>   Gets the liter. </summary>
        ///
        /// <value> The liter. </value>
        public MeasureUnit Liter { get; } 

        /// <summary>   Gets the square meter. </summary>
        ///
        /// <value> The square meter. </value>
        public MeasureUnit SquareMeter { get; } 

        /// <summary>   Gets the cubic meter. </summary>
        ///
        /// <value> The cubic meter. </value>

        public MeasureUnit CubicMeter { get; }

#endregion Measure  Units

    }

}
