using System;

namespace CK.UnitsOfMeasure
{
    /// <summary>
    /// Based on NIST 
    /// http://physics.nist.gov/cgi-bin/cuu/Category?view=html+Frequently+used+constants.x=90+Frequently+used+constants.y=21.
    /// </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/20/2019" by="David" revision="">   Created. </history>
    public static class PhysicsConstants
    {

        /// <summary>   The standard units. </summary>
        private static StandardUnitsContext _standardUnits = StandardUnitsContext.Default;

        /// <summary>
        /// 1.660 538 921 x 10-27 kg
        /// </summary>
        public static Quantity AtomicMass { get; } = new Lazy<Quantity>( () => (1.660538921 * Math.Pow( 10, -27 ).WithUnit( _standardUnits.Kilogram )) ).Value;

        /// <summary>
        /// 6.022 141 29 x 1023 mole-1
        /// </summary>
        public static Quantity Avogadro { get; } =
            new Lazy<Quantity>( () => (6.02214129 * Math.Pow( 10, 23 ).WithUnit( _standardUnits.Unit / _standardUnits.Mole )) ).Value;

        /// <summary>
        /// 1.380 6488 x 10-23 J K-1
        /// </summary>
        public static Quantity Boltzmann { get; } =
            new Lazy<Quantity>( () => 1.3806488 * Math.Pow( 10, -23 ).WithUnit( _standardUnits.Joule / _standardUnits.Kelvin ) ).Value;

        /// <summary>
        /// 8.854 187 817... x 10-12 F m-1
        /// </summary>
        public static Quantity Electric { get; } =
            new Lazy<Quantity>( () => 8.854187817 * Math.Pow( 10, -12 ).WithUnit( _standardUnits.Farad / _standardUnits.Meter ) ).Value;

        /// <summary>
        /// 9.109 382 91 x 10-31 kg
        /// </summary>        
        public static Quantity ElectronMass { get; } =new Lazy<Quantity>( () => 9.10938291 * Math.Pow( 10, -31 ).WithUnit( _standardUnits.Kilogram ) ).Value;

        /// <summary>
        /// 1.602 176 565 x 10-19 J
        /// </summary>
        public static Quantity ElectronVolt { get; } = new Lazy<Quantity>( () => 1.602176565 * Math.Pow( 10, -19 ).WithUnit( _standardUnits.Joule ) ).Value;

        /// <summary>
        /// 1.602 176 565 x 10-19 C
        /// </summary>
        public static Quantity ElementaryCharge { get; } = new Lazy<Quantity>( () => 1.602176565 * Math.Pow( 10, -19 ).WithUnit( _standardUnits.Coulomb ) ).Value;

        /// <summary>
        /// 96 485.3365 C mewl-1
        /// </summary>
        public static Quantity Faraday { get; } = new Lazy<Quantity>( () => 96485.3365.WithUnit( _standardUnits.Coulomb / _standardUnits.Mole ) ).Value;

        /// <summary>
        /// 12.566 370 614... x 10-7 N A-2
        /// </summary>
        public static Quantity Magnetic { get; } =
            new Lazy<Quantity>( () => 12.566370614 * Math.Pow( 10, -7 ).WithUnit( _standardUnits.Newton * (_standardUnits.Ampere ^ -2) ) ).Value;

        /// <summary>
        /// 8.314 4621 J mewl-1 K-1
        /// </summary>
        public static Quantity MolarGas { get; } =
            new Lazy<Quantity>( () => 8.3144621.WithUnit( _standardUnits.Joule * (_standardUnits.Mole ^ -1) * (_standardUnits.Kelvin ^ -1) ) ).Value;

        /// <summary>
        /// 6.673 84 x 10-11 m3 kg-1 s-2
        /// </summary>
        public static Quantity NewtonianGravitation { get; } =
            new Lazy<Quantity>( () => 6.67384 * Math.Pow( 10, -11 ).WithUnit( (_standardUnits.Meter ^ 3) * (_standardUnits.Kilogram ^ -1) * (_standardUnits.Second ^ -2) ) ).Value;

        /// <summary>
        /// 6.626 069 57 x 10-34 J s
        /// </summary>
        public static Quantity Planck { get; } =
            new Lazy<Quantity>( () => 6.62606957 * Math.Pow( 10, -34 ).WithUnit( _standardUnits.Joule * _standardUnits.Second ) ).Value;

        /// <summary>
        /// 1.672 621 777 x 10-27 kg
        /// </summary>
        public static Quantity ProtonMass { get; } =
            new Lazy<Quantity>( () => 1.672621777 * Math.Pow( 10, -27 ).WithUnit( _standardUnits.Kilogram ) ).Value;

        /// <summary>
        /// 10 973 731.568 539 m-1
        /// </summary>
        public static Quantity Rydberg { get; } = new Lazy<Quantity>( () => 10973731.568539.WithUnit( (_standardUnits.Meter ^ -1) ) ).Value;

        /// <summary>
        /// 299 792 458 m s-1
        /// </summary>
        public static Quantity SpeedOfLight { get; } = new Lazy<Quantity>(() => 299792458.WithUnit(_standardUnits.Meter / _standardUnits.Second)).Value;

        /// <summary>
        /// 5.670 373 x 10-8 W m-2 K-4
        /// </summary>
        public static Quantity StefanBoltzmann { get; } =
            new Lazy<Quantity>( () => 5.670373 * Math.Pow( 10, -8 ).WithUnit( _standardUnits.Watt * (_standardUnits.Meter ^ -2) * (_standardUnits.Kelvin ^ -4) ) ).Value;

    }
}
