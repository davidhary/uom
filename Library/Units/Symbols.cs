
using System.Linq;

namespace CK.UnitsOfMeasure
{
    /// <summary> Unit symbols. </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/17/2019" by="David" revision="">   Created. </history>
    public static class UnitSymbols
    {
        // http://unicodelookup.com/#greek
        // http://unicodelookup.com/#math
        // public static char Omega { get { return System.Convert.ToChar( 0x3A9 ); } }
        // public static char Sigma { get { return System.Convert.ToChar( 0x3C3 ); } }
        public static char OmegaInverted { get { return System.Convert.ToChar( 0x2127 ); } }
        // public static char Squared { get { return System.Convert.ToChar( 0x2072 ); } }
        public static char Cubed { get { return System.Convert.ToChar( 0x2073 ); } }
        // public static char Degrees { get { return System.Convert.ToChar( 0x2070 ); } }
        // public static char DotProduct { get { return System.Convert.ToChar( 0x2219 ); } }
        public static char Ppm { get { return System.Convert.ToChar( 0x33D9 ); } }
        public static char PerThousand { get { return System.Convert.ToChar( 0x2030 ); } }
        public static char PerTenThousand { get { return System.Convert.ToChar( 0x2031 ); } }
        public static char WhiteSquare { get { return System.Convert.ToChar( 0x25A1 ); } }
        public static char InvertedOne { get { return System.Convert.ToChar( 0x196 ); } }
        public static int Clamp( int value, int min, int max ) { return (value < min) ? min : (value > max) ? max : value; }
        public static char Subscript( int value ) { checked { value = Clamp( value, 0, 9 ) + 0x2080; } return System.Convert.ToChar( value ); }
        public static char Superscript( int value ) { checked { value = Clamp( value, 0, 9 ) + 0x2070; } return System.Convert.ToChar( value ) ; }
        public const char Degrees = '°';
        public const char Omega = 'Ω';
        public const char Sigma = 'σ';
        public const char Squared = '²';
        public const char Square = '■';
        public const char Dot = '·';
        public const char Pound = '#';
        public const char Percent = '%';
        public const char ForwardSlash = '/';
        private static char[] _validSymbols = new char[] {
            UnitSymbols.OmegaInverted, UnitSymbols.Cubed, UnitSymbols.Ppm, UnitSymbols.PerThousand,
            UnitSymbols.PerTenThousand, UnitSymbols.WhiteSquare, UnitSymbols.InvertedOne,
            UnitSymbols.Subscript(0),
            UnitSymbols.Superscript(1),
            UnitSymbols.Degrees, UnitSymbols.Omega, UnitSymbols.Sigma,
            UnitSymbols.Squared, UnitSymbols.Square, UnitSymbols.Dot,
            UnitSymbols.Pound, UnitSymbols.Percent, UnitSymbols.ForwardSlash

        };
        public static bool IsValidSymbol( char value )
        {
            return _validSymbols.Contains( value );
        }
    }

    /// <summary> Symbols that might be used for unit prefixes. </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/18/2019" by="David" revision="">   Created. </history>

    public static class PrefixSymbols
    {
        // public static char MU { get { return System.Convert.ToChar( 0x3BC ); } }
        // public static char Eta { get { return System.Convert.ToChar( 0x3B7 ); } }
        public const char MU = 'µ';
        public const char Eta = 'η';
    }
}
