using System.Reflection;
using System.Resources;
[assembly: AssemblyCompany("Invenietis")]
[assembly: AssemblyCopyright("(c) 2018 Invenietis. All rights reserved")]
[assembly: AssemblyTrademark("Licensed under The MIT License")]
[assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)]
[assembly: AssemblyCulture("")]

