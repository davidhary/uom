﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle("Invenietis Units of Measure Library")]
[assembly: AssemblyProduct("CK.Units.of.Measure")]
[assembly: AssemblyDescription("Invenietis Units of Measure Library")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
