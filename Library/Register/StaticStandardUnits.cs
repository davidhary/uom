using System;

namespace CK.UnitsOfMeasure
{
    /// <summary>   Specialized <see cref="MeasureContext"/> that defines standard units. </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/21/2019" by="David" revision="">   Created. </history>
    public sealed partial class StandardUnits : MeasureContext
    {
        /// <summary>   Exposes the default context singleton. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
        public static readonly StandardUnits Default = new StandardUnits( String.Empty );

        /// <summary>
        /// Initializes a new <see cref="StandardUnits"/> with a name that should uniquely identify this
        /// context.
        /// </summary>
        ///
        /// <param name="name"> Name of this context. Must not be null or empty: the empty name is
        ///                     reserved for <see cref="Default"/>. </param>

        private StandardUnits( string name ) : base( name, true )
        {
        }

#region . Fundamental Units .

        /// <summary>   Dimensionless unit. Used to count items. Associated symbol is "#". </summary>
        ///
        /// <value> The unit. </value>
        public static FundamentalMeasureUnit Unit { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                Default.DefineFundamental( "#", "Unit", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>
        /// The meter is the length of the path traveled by light in vacuum during a time interval of
        /// 1/299792458 of a second. This is the SI base unit of measure of distance. Associated symbol
        /// is "m".
        /// </summary>
        ///
        /// <value> The meter. </value>
        public static FundamentalMeasureUnit Meter { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "m", "Meter", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>
        /// The gram is our fundamental unit of mass (see <see cref="Kilogram"/>). Associated symbol is
        /// "g".
        /// </summary>
        ///
        /// <value> The gram. </value>
        public static FundamentalMeasureUnit Gram { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "g", "Gram", AutoStandardPrefix.Both, MeasureStandardPrefix.Kilo ) ).Value;

        /// <summary>
        /// The kilogram is the unit of mass; it is equal to the mass of the international prototype of
        /// the kilogram. This is the only SI base unit that includes a prefix. To avoid coping with this
        /// exception in the code, we define it as a <see cref="PrefixedMeasureUnit"/> based on the gram
        /// (<see cref="MeasureStandardPrefix.Kilo"/>
        /// of <see cref="Gram"/>) . Associated symbol is "kg".
        /// </summary>
        ///
        /// <value> The kilogram. </value>
        public static PrefixedMeasureUnit Kilogram { get; } = new Lazy<PrefixedMeasureUnit>( () =>
                                                 (PrefixedMeasureUnit)StandardUnits.Default[$"{MeasureStandardPrefix.Kilo}g"] ).Value;

        /// <summary>
        /// The second is the duration of 9192631770 periods of the radiation corresponding to the
        /// transition between the two hyperfine levels of the ground state of the cesium 133 atom. This
        /// is the SI base unit of measure of time. Associated symbol is "s".
        /// </summary>
        ///
        /// <value> The second. </value>
        public static FundamentalMeasureUnit Second { get ; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "s", "Second", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>
        /// The ampere is that constant current which, if maintained in two straight parallel conductors
        /// of infinite length, of negligible circular cross-section, and placed 1 metre apart in vacuum,
        /// would produce between these conductors a force equal to 2×10−7 newton per metre of length.
        /// This is the SI base unit of measure of electric current. Associated symbol is "A".
        /// </summary>
        ///
        /// <value> The ampere. </value>
        public static FundamentalMeasureUnit Ampere { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "A", "Ampere", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>
        /// The kelvin, unit of thermodynamic temperature, is the fraction 1 / 273.16 of the
        /// thermodynamic temperature of the triple point of water. This is the SI base unit of measure
        /// of thermodynamic temperature. Associated symbol is "K".
        /// </summary>
        ///
        /// <value> The kelvin. </value>
        public static FundamentalMeasureUnit Kelvin { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "K", "Kelvin", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>
        /// The mole is the amount of substance of a system which contains as many elementary entities as
        /// there are atoms in 0.012 kilogram of carbon 12; its symbol is 'mol'. This is the SI base unit
        /// of measure of an amount of substance. Associated symbol is "mol".
        /// </summary>
        ///
        /// <value> The mole. </value>
        public static FundamentalMeasureUnit Mole { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "mol", "Mole", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>
        /// The candela is the luminous intensity, in a given direction, of a source that emits
        /// monochromatic radiation of frequency 540×1012 hertz and that has a radiant intensity in that
        /// direction of 1/683 watt per steradian. This is the SI base unit of measure of luminous
        /// intensity. Associated symbol is "cd".
        /// </summary>
        ///
        /// <value> The candela. </value>
        public static FundamentalMeasureUnit Candela { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "cd", "Candela", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>
        /// A bit is defined as the information entropy of a binary random variable that is 0 or 1 with
        /// equal probability. Associated symbol is "b" (recommended by the IEEE 1541-2002 and IEEE Std
        /// 260.1-2004 standards).
        /// </summary>
        ///
        /// <value> The bit. </value>
        public static FundamentalMeasureUnit Bit { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( "b", "Bit", AutoStandardPrefix.Both ) ).Value;

        /// <summary>   Gets the centigrade. </summary>
        ///
        /// <value> The centigrade. </value>
        public static FundamentalMeasureUnit Centigrade { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( $"{UnitSymbols.Degrees}C", "Centigrade", AutoStandardPrefix.None ) ).Value;

        /// <summary>   Gets the Fahrenheit. </summary>
        ///
        /// <value> The Fahrenheit unit. </value>
        public static FundamentalMeasureUnit Fahrenheit { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( $"{UnitSymbols.Degrees}F", "Fahrenheit", AutoStandardPrefix.None ) ).Value;

        /// <summary>
        /// The Neper (unit symbol Np) is a logarithmic unit for ratios of measurements of physical field
        /// and power quantities, such as gain and loss of electronic signals. The unit's name is derived
        /// from the name of John Napier, the inventor of logarithms.
        /// </summary>
        ///
        /// <value> The neper. </value>
        public static FundamentalMeasureUnit Neper { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( $"nP", "Neper", AutoStandardPrefix.None ) ).Value;

        /// <summary>
        /// The Neper (unit symbol Np) is a logarithmic unit for ratios of measurements of physical field
        /// and power quantities, such as gain and loss of electronic signals. The unit's name is derived
        /// from the name of John Napier, the inventor of logarithms.
        /// </summary>
        ///
        /// <value> The bel. </value>
        public static FundamentalMeasureUnit Bel { get; } = new Lazy<FundamentalMeasureUnit>( () =>
                                                 Default.DefineFundamental( $"Bel", "Bel", AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   The kilograms. </summary>
        private readonly static PrefixedMeasureUnit _kg = Kilogram;
        /// <summary>   The m. </summary>
        private readonly static FundamentalMeasureUnit _m = Meter;
        /// <summary>   The s. </summary>
        private readonly static FundamentalMeasureUnit _s = Second;
        /// <summary>   a. </summary>
        private readonly static FundamentalMeasureUnit _a = Ampere;

#endregion Fundamental Units

#region . Atomic Units .

        /// <summary>   Gets the decimeter. </summary>
        ///
        /// <value> The decimeter. </value>
        public static AtomicMeasureUnit Decimeter { get; } = new Lazy<AtomicMeasureUnit>( () => MeasureStandardPrefix.Deci[Meter] ).Value;

        /// <summary>   Gets the kilometer. </summary>
        ///
        /// <value> The kilometer. </value>
        public static AtomicMeasureUnit Kilometer { get; } = new Lazy<AtomicMeasureUnit>( () => MeasureStandardPrefix.Kilo[Meter] ).Value;

        /// <summary>
        /// Micron is a length measurement unit. micron is an unofficial name for micrometer (μm).
        /// </summary>
        ///
        /// <value> The micron. </value>
        public static AtomicMeasureUnit Micron { get; } = new Lazy<AtomicMeasureUnit>( () => MeasureStandardPrefix.Micro[Meter] ).Value;

        /// <summary>   Micrometer (μm). </summary>
        ///
        /// <value> The micro meter. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "MicroMeter" )]
        public static AtomicMeasureUnit MicroMeter { get; } = new Lazy<AtomicMeasureUnit>( () => MeasureStandardPrefix.Micro[Meter] ).Value;

        #endregion Atomic Units

#region . Alias Units .

        /// <summary>
        /// A byte is now standardized as eight bits, as documented in ISO/IEC 2382-1:1993. The
        /// international standard IEC 80000-13 codified this common meaning. Associated symbol is "B"
        /// and it is an alias with a <see cref="ExpFactor"/> of 2^3 on <see cref="Bit"/>.
        /// </summary>
        ///
        /// <value> The byte. </value>
        public static AliasMeasureUnit Byte { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "B", "Byte", new FullFactor( new ExpFactor( 3, 0 ) ), Bit, AutoStandardPrefix.Both ) ).Value;

        /// <summary>   Gets the percent; parts per one hundred. </summary>
        ///
        /// <value> The percent; parts per one hundred. </value>
        public static AliasMeasureUnit Percent { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "%", "Percent", new ExpFactor( 0, -2 ), MeasureUnit.None ) ).Value;

        /// <summary>   Gets the permille; parts per one thousand. </summary>
        ///
        /// <value> The permille; parts per one thousand. </value>
        public static AliasMeasureUnit Permille { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"{UnitSymbols.PerThousand}" , "Permille", new ExpFactor( 0, -3 ), MeasureUnit.None ) ).Value;

        /// <summary>   Gets the permyriad; parts per one ten thousand. </summary>
        ///
        /// <value> The permyriad; parts per ten thousand. </value>
        public static AliasMeasureUnit Permyriad{ get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"{UnitSymbols.PerTenThousand}", "Permyriad", new ExpFactor( 0, -4 ), MeasureUnit.None ) ).Value;

        /// <summary>   Gets the ppm; parts per million. </summary>
        ///
        /// <value> The ppm; parts per million. </value>
        public static AliasMeasureUnit Ppm { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                  Default.DefineAlias( $"{UnitSymbols.Ppm}", "PPM", new ExpFactor( 0, -6 ), MeasureUnit.None ) ).Value;


        /// <summary>   Gets the inch. </summary>
        ///
        /// <value> The inch. </value>

        public static AliasMeasureUnit Inch { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "in", "Inch", 0.0254, Meter ) ).Value;

        /// <summary>   Gets the foot. </summary>
        ///
        /// <value> The foot. </value>
        public static AliasMeasureUnit Foot { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "ft", "Foot", 12, Inch ) ).Value;

        /// <summary>   Gets the yard. </summary>
        ///
        /// <value> The yard. </value>
        public static AliasMeasureUnit Yard { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "yd", "Yard", 36, Inch ) ).Value;

        /// <summary>   Gets the mile. </summary>
        ///
        /// <value> The mile. </value>
        public static AliasMeasureUnit Mile { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "mi", "Mile", 5280, Foot ) ).Value;

        /// <summary>   Gets the nautical mile. </summary>
        ///
        /// <value> The nautical mile. </value>
        public static AliasMeasureUnit NauticalMile { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "nmi", "Nautical Mile", 1852, Meter ) ).Value;

        /// <summary>   Gets the lightyear. </summary>
        ///
        /// <value> The lightyear. </value>
        public static AliasMeasureUnit Lightyear { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "ly", "Lightyear", 9460730472580800.0, Meter ) ).Value;

        /// <summary>   Gets the gallon. </summary>
        ///
        /// <value> The gallon. </value>
        public static AliasMeasureUnit Gallon { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "gal", "Gallon", 231, Inch ^ 3 ) ).Value;

        /// <summary>   Gets the MCF. </summary>
        ///
        /// <value> The MCF. </value>
        public static AliasMeasureUnit MCF { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "MCF", "MCF", 1000, Foot ) ).Value;

        /// <summary>   Gets the MMCF. </summary>
        ///
        /// <value> The MMCF. </value>
        public static AliasMeasureUnit MMCF { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "MMCF", "MMCF", 1000000, Foot ) ).Value; 

        /// <summary>   Gets the minute. </summary>
        ///
        /// <value> The minute. </value>
        public static AliasMeasureUnit Minute { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "min", "Minute", 60 , Second ) ).Value;

        /// <summary>   Gets the hour. </summary>
        ///
        /// <value> The hour. </value>
        public static AliasMeasureUnit Hour { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "h", "Hour", 3600, Second ) ).Value;

        /// <summary>   Gets the day. </summary>
        ///
        /// <value> The day. </value>
        public static AliasMeasureUnit Day { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "d", "Day", 24, Hour ) ).Value;

        /// <summary>   Gets the knot. </summary>
        ///
        /// <value> The knot. </value>
        public static AliasMeasureUnit Knot { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "kn", "Knot", 1.852, Kilometer / Hour ) ).Value;

        /// <summary>   Gets the newton. </summary>
        ///
        /// <value> The newton. </value>
        public static AliasMeasureUnit Newton { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "N", "Newton", FullFactor.Neutral, _kg * _m * (_s ^ -2), AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the dyne. </summary>
        ///
        /// <value> The dyne. </value>
        public static AliasMeasureUnit Dyne { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "dyn", "Dyne", new ExpFactor( 0, -5 ), Newton, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the kilopond. </summary>
        ///
        /// <value> The kilopond. </value>
        public static AliasMeasureUnit Kilopond { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "kp", "Kilopond", 9.80665, Newton ) ).Value;

        /// <summary>   Gets the pound. </summary>
        ///
        /// <value> The pound. </value>
        public static AliasMeasureUnit Pound { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "lbf", "Pound", 4.4482216, Newton ) ).Value;

        /// <summary>   Gets the joule. </summary>
        ///
        /// <value> The joule. </value>
        public static AliasMeasureUnit Joule { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "J", "Joule", FullFactor.Neutral, _kg * (_m ^ 2) * (_s ^ -2), AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the watt. </summary>
        ///
        /// <value> The watt. </value>
        public static AliasMeasureUnit Watt { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "W", "Watt", FullFactor.Neutral, Joule / _s, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the calorie. </summary>
        ///
        /// <value> The calorie. </value>
        public static AliasMeasureUnit Calorie { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "cal", "Calorie", 4.1868, Joule, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the horsepower. </summary>
        ///
        /// <value> The horsepower. </value>
        public static AliasMeasureUnit Horsepower { get; } = new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( "hp", "Horsepower", 735.49875, Watt ) ).Value;

        /// <summary>   Gets the coulomb. </summary>
        ///
        /// <value> The coulomb. </value>
        public static AliasMeasureUnit Coulomb { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "C", "Coulomb", FullFactor.Neutral, _a * _s, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the volt. </summary>
        ///
        /// <value> The volt. </value>
        public static AliasMeasureUnit Volt { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( "V", "Volt", FullFactor.Neutral, Watt / _a, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the ohm. </summary>
        ///
        /// <value> The ohm. </value>
        public static AliasMeasureUnit Ohm { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"{UnitSymbols.Omega}", "Ohm", FullFactor.Neutral, Volt / _a, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the mho. </summary>
        ///
        /// <value> The mho. </value>
        public static AliasMeasureUnit Mho { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"{UnitSymbols.OmegaInverted}", "Mho", FullFactor.Neutral, _a / Volt, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the farad. </summary>
        ///
        /// <value> The farad. </value>
        public static AliasMeasureUnit Farad { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"F", "Farad", FullFactor.Neutral, Coulomb / Volt, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the henry. </summary>
        ///
        /// <value> The henry. </value>
        public static AliasMeasureUnit Henry { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"H", "Henry", FullFactor.Neutral, Ohm * _s, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the seebeck. </summary>
        ///
        /// <value> The seebeck. </value>
        public static AliasMeasureUnit Seebeck { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"V/{UnitSymbols.Degrees}", "Seebeck", FullFactor.Neutral, Volt / Kelvin, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the ohm per square. </summary>
        ///
        /// <value> The ohm per square. </value>
        public static AliasMeasureUnit OhmPerSquare { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"{UnitSymbols.Omega}/{UnitSymbols.WhiteSquare}", "Ohm/sq", FullFactor.Neutral, Ohm * _m, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the pascal. </summary>
        ///
        /// <value> The pascal. </value>
        public static AliasMeasureUnit Pascal { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"Pa", "Pascal", FullFactor.Neutral, Newton * (_m ^ -2), AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the bar. </summary>
        ///
        /// <value> The bar. </value>
        public static AliasMeasureUnit Bar { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"bar", "Bar", 100000, Pascal, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the atmosphere. </summary>
        ///
        /// <value> The atmosphere. </value>
        public static AliasMeasureUnit Atmosphere { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"atm", "Atmosphere", 101325, Pascal, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Pound-force (lbf) per square inch. </summary>
        ///
        /// <value> The psi. </value>
        public static AliasMeasureUnit PSI { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"psi", "PSI", 6894.7, Pascal, AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the inch of water column unit. </summary>
        ///
        /// <value> The inch of water column unit. </value>
        public static AliasMeasureUnit WaterColumn { get; } =
            new Lazy<AliasMeasureUnit>( () => Default.DefineAlias( $"wc", "Water Column", 249.088908333, Pascal, AutoStandardPrefix.None ) ).Value;

        /// <summary>   Gets the hertz. </summary>
        ///
        /// <value> The hertz. </value>
        public static AliasMeasureUnit Hertz { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"Hz", "Hertz", FullFactor.Neutral, Unit * (_s ^ -1), AutoStandardPrefix.Metric ) ).Value;

        /// <summary>   Gets the rpm. </summary>
        ///
        /// <value> The rpm. </value>
        public static AliasMeasureUnit RPM { get; } = new Lazy<AliasMeasureUnit>( () =>
                                                 Default.DefineAlias( $"RPM", "RPM", FullFactor.Neutral, Unit * (Minute ^ -1), AutoStandardPrefix.Metric ) ).Value;

#endregion Alias Units

#region . Measure Units .

        /// <summary>   Gets the ohm meter. </summary>
        ///
        /// <value> The ohm meter. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "OhmMeter" )]
        public static MeasureUnit OhmMeter { get; } = new Lazy<MeasureUnit>( () => Ohm * Meter ).Value;

        /// <summary>   Gets the cubic foot per hour. </summary>
        ///
        /// <value> The cubic foot per hour. </value>
        public static MeasureUnit CubicFootPerHour { get; } = new Lazy<MeasureUnit>( () => (Foot ^ 3) / Hour ).Value;

        /// <summary>   Gets the MCF per day. </summary>
        ///
        /// <value> The MCF per day. </value>
        public static MeasureUnit MCFPerDay { get; } = new Lazy<MeasureUnit>( () => MCF / Day ).Value;

        /// <summary>   Gets the MMCF per day. </summary>
        ///
        /// <value> The MMCF per day. </value>
        public static MeasureUnit MMCFPerDay { get; } = new Lazy<MeasureUnit>( () => MMCF / Day ).Value;

        /// <summary>   Gets the kilometer per hour. </summary>
        ///
        /// <value> The kilometer per hour. </value>
        public static MeasureUnit KilometerPerHour { get; } = new Lazy<MeasureUnit>( () => Kilometer / Hour ).Value;

        /// <summary>   Gets the liter. </summary>
        ///
        /// <value> The liter. </value>
        public static MeasureUnit Liter { get; } = new Lazy<MeasureUnit>( () => Decimeter ^ 3 ).Value;

        /// <summary>   Gets the square meter. </summary>
        ///
        /// <value> The square meter. </value>
        public static MeasureUnit SquareMeter { get; } = new Lazy<MeasureUnit>( () => Meter ^ 2 ).Value;

        /// <summary>   Gets the cubic meter. </summary>
        ///
        /// <value> The cubic meter. </value>

        public static MeasureUnit CubicMeter { get; } = new Lazy<MeasureUnit>( () => Meter ^ 3 ).Value;

#endregion Measure  Units

    }
}
