using System;

namespace CK.UnitsOfMeasure
{
	/// <summary>
	/// Attribute to mark classes having static unit fields to be registered
	/// by the UnitManager's RegisterUnits method.
	/// </summary>
	/// <see cref="UnitRegister.RegisterUnits(System.Reflection.Assembly)"/>
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class UnitDefinitionClassAttribute : Attribute
	{
	}
}
