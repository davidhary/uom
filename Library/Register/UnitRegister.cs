using System;
using System.Collections.Generic;
using System.Reflection;

namespace CK.UnitsOfMeasure
{
    /// <summary>
    /// Registers the units.
    /// </summary>
    public sealed partial class UnitRegister
    {

        #region . Singleton .

        private static UnitRegister _instance;

        /// <summary>
        /// The instance of the currently used UnitManager.
        /// </summary>
        public static UnitRegister Instance
        {
            get
            {
                if( UnitRegister._instance == null )
                {
                    UnitRegister._instance = new UnitRegister();
                }
                return UnitRegister._instance;
            }
            set { UnitRegister._instance = value; }
        }

        #endregion Singleton

        #region . Collections .

        // Stores for registered units:
        private List<MeasureUnit> _registeredUnits = new List<MeasureUnit>();

        /// <summary>   The units by symbol. </summary>
        /// <remarks>
        /// DH: set the dictionary to ignore case on name but not on symbol (e.g., difference between
        /// Mega Ohm and Milli Ohm)
        /// </remarks>
        private Dictionary<string, MeasureUnit> _unitsByName = new Dictionary<string, MeasureUnit>( StringComparer.OrdinalIgnoreCase );

        /// <summary>   The units by symbol. </summary>
        private Dictionary<string, MeasureUnit> _unitsBySymbol = new Dictionary<string, MeasureUnit>();

        /// <summary>
        /// Returns all registered units.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate" )]
        public static IList<MeasureUnit> GetUnits()
        {
            return Instance._registeredUnits;
        }

        /// <summary>
        /// Whether the given unit is already registered to the UnitManager.
        /// </summary>
        public static bool IsRegistered( MeasureUnit unit )
        {
            return (Instance._registeredUnits.Contains( unit ));
        }

#endregion Collections 

#region . Registrations

        /// <summary>
        /// Registers a unit.
        /// </summary>
        public static void RegisterUnit( MeasureUnit unit )
        {
            // Check precondition: unit <> null
            if( unit is null ) throw new ArgumentNullException( nameof( unit ) );

            // Check if unit already registered:
            foreach( MeasureUnit u in Instance._registeredUnits )
            {
                if( Object.ReferenceEquals( u, unit ) ) return;

            }

            // Register unit in allUnits:
            Instance._registeredUnits.Add( unit );

            // Register unit by name and symbol:
            if( !Instance._unitsByName.ContainsKey( unit.Name ) ) Instance._unitsByName.Add( unit.Name, unit );
            if( !Instance._unitsBySymbol.ContainsKey( unit.Symbol ) ) Instance._unitsBySymbol.Add( unit.Symbol, unit );
            Instance._unitsByName[unit.Name] = unit;
            Instance._unitsBySymbol[unit.Symbol] = unit;
        }

        /// <summary>
        /// Register all public static fields of type MeasureUnit of the given class.
        /// </summary>
        public static void RegisterUnits( Type unitDefinitionClass )
        {
            if( unitDefinitionClass == null ) throw new ArgumentNullException( nameof( unitDefinitionClass ) );
            foreach( FieldInfo member in unitDefinitionClass.GetFields( BindingFlags.GetField | BindingFlags.Public | BindingFlags.Static ) )
            {
                if( member.FieldType.IsSubclassOf( typeof( MeasureUnit ) ) )
                {
                    MeasureUnit unit = (MeasureUnit)member.GetValue( null );
                    if( unit == null ) throw new InvalidOperationException( $"{member} unit is not initialized" );
                    UnitRegister.RegisterUnit( unit );
                }
            }
            foreach( PropertyInfo member in unitDefinitionClass.GetProperties( BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Static ) )
            {

                if( member.PropertyType.Name.StartsWith( "MeasureUnit" , StringComparison.OrdinalIgnoreCase ) )
                {
                    MeasureUnit unit = (MeasureUnit)member.GetValue( null );
                    if( unit == null ) throw new InvalidOperationException( $"{member} unit is not initialized" );
                    UnitRegister.RegisterUnit( unit );
                }
                else if( member.PropertyType.Name.StartsWith( "MeasureUnit", StringComparison.OrdinalIgnoreCase ) ||
                    member.PropertyType.IsSubclassOf( typeof( MeasureUnit ) ) )
                {
                    MeasureUnit unit = (MeasureUnit)member.GetValue( null );
                    if( unit == null ) throw new InvalidOperationException( $"{member} unit is not initialized" );
                    UnitRegister.RegisterUnit( unit );
                }
            }
        }

        /// <summary>
        /// Registers all public static fields of type MeasureUnit of classes
        /// marked with the [UnitDefinitionClass] attribute in the given
        /// assembly.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0" )]
        public static void RegisterUnits( Assembly assembly )
        {
            foreach( Type t in assembly.GetExportedTypes() )
            {
                if( t.GetCustomAttributes( typeof( UnitDefinitionClassAttribute ), false ).Length > 0 )
                {
                    RegisterUnits( t );
                }
            }
        }

#endregion Registrations

#region . Selectors .

        /// <summary>
        /// Retrieves the unit based on its name.
        /// If the unit is not found, a UnitResolve event is fired as last chance
        /// to resolve the unit.
        /// If the unit cannot be resolved, an UnknownUnitException is raised.
        /// </summary>
        public static MeasureUnit GetUnitByName( string name )
        {
            // Try resolve unit by unitsByName:
            Instance._unitsByName.TryGetValue( name, out MeasureUnit result );

            // Throw exception if unit resolution failed:
            if( result == null )
            {
                throw new UnknownUnitException( string.Format( $"No unit found named '{name}'." ) );
            }

            // Return result:
            return result;
        }

        /// <summary>
        /// Retrieves the unit based on its symbol.
        /// If the unit is not found, an UnknownUnitException is raised.
        /// </summary>
        public static MeasureUnit GetUnitBySymbol( string symbol )
        {
            // Try resolve unit by unitsBySymbol:
            Instance._unitsBySymbol.TryGetValue( symbol, out MeasureUnit result );

            // Throw exception if unit resolution failed:
            if( result == null )
            {
                throw new UnknownUnitException( string.Format( $"No unit found with symbol '{symbol}'."  ) );

            }

            // Return result:
            return result;
        }

#endregion Selectors

    }
}

