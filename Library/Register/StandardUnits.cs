using System;

namespace CK.UnitsOfMeasure
{
    /// <summary>   Specialized <see cref="MeasureContext"/> that defines standard units. </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/21/2019" by="David" revision="">   Created. </history>
	[UnitDefinitionClass]
    public static class StandardUnits 
    {
        /// <summary>   Exposes the default context singleton. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes" )]
        private static readonly Lazy<StandardUnitsContext> _default = new Lazy<StandardUnitsContext>( () => StandardUnitsContext.Default );

        // accessor for the default context singleton
        private static StandardUnitsContext Default
        {
            get
            {
                return _default.Value;
            }
        }

#if false
    public sealed partial class StandardUnits : MeasureContext
        /// <summary>
        /// Initializes a new <see cref="StandardUnits"/> with a name that should uniquely identify this
        /// context.
        /// </summary>
        ///
        /// <param name="name"> Name of this context. Must not be null or empty: the empty name is
        ///                     reserved for <see cref="Default"/>. </param>
        private StandardUnits( string name ) : base( name, true )
        {
        }

#endif
        public static FundamentalMeasureUnit Unit { get; } = Default.Unit ;
        public static FundamentalMeasureUnit Meter { get; } = Default.Meter ;
        public static FundamentalMeasureUnit Gram { get; } = Default.Gram;
        public static PrefixedMeasureUnit Kilogram { get; } = Default.Kilogram;
        public static FundamentalMeasureUnit Second { get; } = Default.Second;
        public static FundamentalMeasureUnit Ampere { get; } = Default.Ampere;
        public static FundamentalMeasureUnit Kelvin { get; } = Default.Kelvin;
        public static FundamentalMeasureUnit Mole { get; } = Default.Mole;
        public static FundamentalMeasureUnit Candela { get; } = Default.Candela;
        public static FundamentalMeasureUnit Bit { get; } = Default.Bit;
        public static FundamentalMeasureUnit Centigrade { get; } = Default.Centigrade;
        public static FundamentalMeasureUnit Fahrenheit { get; } = Default.Fahrenheit; 
        public static FundamentalMeasureUnit Neper { get; } = Default.Neper ; 
        public static FundamentalMeasureUnit Bel { get; } = Default.Bel ; 
        public static AtomicMeasureUnit Decimeter { get; } = Default.Decimeter;
        public static AtomicMeasureUnit Kilometer { get; } = Default.Kilometer;
        public static AtomicMeasureUnit Micron { get; } = Default.Micron; 
        public static AtomicMeasureUnit Micrometer { get; } = Default.Micrometer ; 
        public static AliasMeasureUnit Byte { get; } = Default.Byte ;
        public static AliasMeasureUnit Percent { get; } = Default.Percent ;
        public static AliasMeasureUnit Permille { get; } = Default.Permille ;
        public static AliasMeasureUnit Permyriad{ get; } = Default.Permyriad ;
        public static AliasMeasureUnit Ppm { get; } = Default.Ppm ; 
        public static AliasMeasureUnit Inch { get; } = Default.Inch ;
        public static AliasMeasureUnit Foot { get; } = Default.Foot ;
        public static AliasMeasureUnit Yard { get; } = Default.Yard ;
        public static AliasMeasureUnit Mile { get; } = Default.Mile ;
        public static AliasMeasureUnit NauticalMile { get; } = Default.NauticalMile ; 
        public static AliasMeasureUnit Lightyear { get; } = Default.Lightyear ; 
        public static AliasMeasureUnit Gallon { get; } = Default.Gallon ; 
        public static AliasMeasureUnit MCF { get; } = Default.MCF ; 
        public static AliasMeasureUnit MMCF { get; } = Default.MMCF ;
        public static AliasMeasureUnit Minute { get; } = Default.Minute ;
        public static AliasMeasureUnit Hour { get; } = Default.Hour ; 
        public static AliasMeasureUnit Day { get; } = Default.Day ; 
        public static AliasMeasureUnit Knot { get; } = Default.Knot ;
        public static AliasMeasureUnit Newton { get; } = Default.Newton ;
        public static AliasMeasureUnit Gravity { get; } = Default.Gravity;
        public static AliasMeasureUnit Dyne { get; } = Default.Dyne ; 
        public static AliasMeasureUnit Kilopond { get; } = Default.Kilopond ;
        public static AliasMeasureUnit KilogramForce { get; } = Default.KilogramForce;
        public static AliasMeasureUnit Pound { get; } = Default.Pound ; 
        public static AliasMeasureUnit Joule { get; } = Default.Joule ; 
        public static AliasMeasureUnit Watt { get; } = Default.Watt ; 
        public static AliasMeasureUnit Calorie { get; } = Default.Calorie ; 
        public static AliasMeasureUnit Horsepower { get; } = Default.Horsepower ;
        public static AliasMeasureUnit Coulomb { get; } = Default.Coulomb ; 
        public static AliasMeasureUnit Volt { get; } = Default.Volt ; 
        public static AliasMeasureUnit Ohm { get; } = Default.Ohm ; 
        public static AliasMeasureUnit Mho { get; } = Default.Mho ; 
        public static AliasMeasureUnit Farad { get; } = Default.Farad ;
        public static AliasMeasureUnit Henry { get; } = Default.Henry ;
        public static AliasMeasureUnit Seebeck { get; } = Default.Seebeck ;
        public static AliasMeasureUnit OhmPerSquare { get; } = Default.OhmPerSquare ; 
        public static AliasMeasureUnit Pascal { get; } = Default.Pascal ; 
        public static AliasMeasureUnit Bar { get; } = Default.Bar ; 
        public static AliasMeasureUnit Atmosphere { get; } = Default.Atmosphere ; 
        public static AliasMeasureUnit PSI { get; } = Default.PSI ; 
        public static AliasMeasureUnit WaterColumn { get; } =  Default.WaterColumn ;
        public static AliasMeasureUnit Hertz { get; } = Default.Hertz; 
        public static AliasMeasureUnit RPM { get; } = Default.RPM ;
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "OhmMeter" )]
        public static MeasureUnit OhmMeter { get; } = Default.OhmMeter ; 
        public static MeasureUnit CubicFootPerHour { get; } = Default.CubicFootPerHour ;
        public static MeasureUnit MCFPerDay { get; } = Default.MCFPerDay ; 
        public static MeasureUnit MMCFPerDay { get; } = Default.MMCFPerDay ;
        public static MeasureUnit KilometerPerHour { get; } = Default.KilometerPerHour ; 
        public static MeasureUnit Liter { get; } = Default.Liter ; 
        public static MeasureUnit SquareMeter { get; } = Default.SquareMeter ; 
        public static MeasureUnit CubicMeter { get; } = Default.CubicMeter ; 
    }
}
