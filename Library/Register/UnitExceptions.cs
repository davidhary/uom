using System;
using System.Runtime.Serialization;

namespace CK.UnitsOfMeasure
{

	/// <summary>
	/// Exception thrown whenever an exception is referenced by name, but no
	/// unit with the given name is known (registered to the UnitManager).
	/// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1032:ImplementStandardExceptionConstructors"), Serializable]
	public class UnknownUnitException : Exception
	{
		public UnknownUnitException() : base() { }

		public UnknownUnitException(string message) 
            : base(message) { }

		protected UnknownUnitException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{ }
	}
}
