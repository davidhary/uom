# UoM: Units of Measure Project

Supports implementation of units of measure

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
```
git clone git@bitbucket.org:davidhary/uom.git
```

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* **spi, Invenietis** - *Creator* - https://www.codeproject.com/Members/spi)
* **David Hary** - *.Net Framwork packaging* - [ATE Coder](https://www.IntegratedScientificResources.com)

## Original Source Code

* **CK-Units of Measure, Invenietis** - https://github.com/Invenietis/CK-UnitsOfMeasure 

## License

This repository is licensed under the [MIT License](https://www.bitbucket.org/davidhary/uom/src/master/LICENSE.md)

## Acknowledgments

* [spi] {https://www.codeproject.com/Articles/1259458/A-Dynamic-Units-of-Measure-Library-in-9-Days) - A Dynamic Units of Measure Library in 9 Days
* [Rudi Breedenraedt] {https://www.codeproject.com/Articles/611731/Working-with-Units-and-Amounts) - Working with Units and Amounts
* [Stack overflow](https://www.stackoveflow.com)

## Revision Changes

* Version 0.1.7076	Packaged as a .Net framwork solution. Added electrical units and abbreviations.
