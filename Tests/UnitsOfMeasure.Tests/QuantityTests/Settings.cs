
namespace CK.UnitsOfMeasure.MyTests
{
    internal sealed partial class Settings : global::My.ApplicationSettingsBase
    {

        private static Settings _defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized( new Settings() )));

        public static Settings Default
        {
            get
            {
                return _defaultInstance;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode" )]
        [System.Configuration.UserScopedSettingAttribute()]
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.Configuration.DefaultSettingValue( "299792458" )]
        public double SpeedOfLight
        {
            get => ReadAppSettingDouble();
            set => WriteAppSetting( value );
        }
    }
}
