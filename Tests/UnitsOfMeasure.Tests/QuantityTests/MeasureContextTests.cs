using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.UnitsOfMeasure.MyTests
{
    /// <summary>   (MeasureUnit Test Fixture) a measure context tests. </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/22/2019" by="David" revision="">   Created. </history>

    [TestFixture]
    public class MeasureContextTests
    {

#region . Initialize & cleanup.

        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }


        /// <summary>   The standard units. </summary>
        private static StandardUnitsContext _standardUnitsContext = StandardUnitsContext.Default;

#endregion Initialize & cleanup

#region . Physics Constants .

        [Test]
        public void SpeedOfLightTest()
        {
            Quantity speedOfLight = PhysicsConstants.SpeedOfLight;
            speedOfLight.Should().Be( Settings.Default.SpeedOfLight.WithUnit( _standardUnitsContext.Meter / _standardUnitsContext.Second ) );
        }


#endregion Physical Constants

#region . Standard Units .

        [Test]
        public void ValidSymbolTest()
        {
            string testSymbol = $"V/{UnitSymbols.Degrees}";
            StandardMeasureContext.Default.IsValidNewSymbol( testSymbol,  AutoStandardPrefix.Metric ).Should().Be( true );
            testSymbol = "inH2O";
            StandardMeasureContext.Default.IsValidNewSymbol( testSymbol, AutoStandardPrefix.None ).Should().Be( false , $"{testSymbol} includes a number" );
            testSymbol = "inHO";
            StandardMeasureContext.Default.IsValidNewSymbol( testSymbol, AutoStandardPrefix.None ).Should().Be( true, $"{testSymbol} is valid" );
            testSymbol = "wc";
            StandardMeasureContext.Default.IsValidNewSymbol( testSymbol, AutoStandardPrefix.None ).Should().Be( true , $"{testSymbol} is valid" );
        }

        /// <summary>   Tests quantity construction by symbol. </summary>
        [Test]
        public void QuantityConstructionBySymbolTest()
        {
            string unitSymbol = _standardUnitsContext.Liter.Symbol;
            StandardUnitsContext.Default.TryParse( unitSymbol , out MeasureUnit u ).Should().Be( true, $"{unitSymbol} was registered" );
            double value = 100;
            Quantity a = new Quantity( value , u );
            a.Value.Should().Be( value , $"quantity value was set to {value}");
            a.Unit.Symbol.Should().Be( unitSymbol, $"MeasureUnit symbol was assigned to {unitSymbol}" );
        }

        #endregion Standard Units

    }
}


