using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK.UnitsOfMeasure.MyTests
{
    /// <summary>   (Unit Test Fixture) a standard units tests. </summary>
    ///
    /// <license>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    ///
    /// <history date="5/22/2019" by="David" revision="">   Created. </history>

    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors" )]
    [TestFixture]
    public class StandardUnitsTests
    {

#region . Initialize & cleanup .

        private static UnitRegister _defaultUnitRegister;

        [SetUp]
        public static void SetUp()
        {
            Console.Write( "Resetting the Unit Register instance..." );
            StandardUnitsTests._defaultUnitRegister = UnitRegister.Instance;
            UnitRegister.Instance = new UnitRegister();
            UnitRegister.RegisterUnits( typeof( CK.UnitsOfMeasure.PhysicsConstants ).Assembly );
            Console.WriteLine( " done." );
        }

        [TearDown]
        public static void TearDown()
        {
            UnitRegister.Instance = StandardUnitsTests._defaultUnitRegister;
        }

        /// <summary>   The standard units. </summary>
        private static StandardUnitsContext _standardUnitsContext = StandardUnitsContext.Default;

#endregion Initialize & cleanup

#region . Physics Constants .

        /// <summary>   Tests speed of light. </summary>
        [Test]
        public static void SpeedOfLightTest()
        {
            Quantity speedOfLight = PhysicsConstants.SpeedOfLight;
            speedOfLight.Should().Be( Settings.Default.SpeedOfLight.WithUnit( _standardUnitsContext.Meter / _standardUnitsContext.Second ) );
        }

#endregion Physical Constants

#region . Units by Name .

        /// <summary>   Tests select unit by name. </summary>
        [Test]
        public static void SelectUnitByNameTest()
        {
            FundamentalMeasureUnit unitFromText = (FundamentalMeasureUnit)UnitRegister.GetUnitByName( "Unit" );
            unitFromText.Should().Be( StandardUnits.Unit );
        }

#endregion Units by Name

#region . Quantity Math .

        /// <summary>   Tests addition. </summary>
        [Test]
        public static void AdditionTest()
        {
            Quantity a = new Quantity( 3000.0, _standardUnitsContext.Meter );
            Quantity b = new Quantity( 2000.0, _standardUnitsContext.Meter );
            Quantity expected = new Quantity( 5000.0, _standardUnitsContext.Meter );
            (a + b).Should().Be( expected , $"equals to {a.Value}+{b.Value}");
        }

        /// <summary>   Tests derived unit addition. </summary>
        [Test]
        public static void DerivedUnitAdditionTest()
        {
            Quantity a = new Quantity( 3000.0, _standardUnitsContext.Meter );
            Quantity b = new Quantity( 2.0, _standardUnitsContext.Kilometer );
            Quantity expected = new Quantity( 5.0, _standardUnitsContext.Kilometer );
            (a + b).Should().Be( expected, $"equals to {a.Value}{a.Unit.Symbol}+{b.Value}{b.Unit.Symbol}" );
        }

        /// <summary>   Tests speed time distance. </summary>
        [Test]
        public static void SpeedTimeDistanceTest()
        {
            Quantity speed = new Quantity(120, _standardUnitsContext.Kilometer / _standardUnitsContext.Hour);
            Quantity time = new Quantity(15, _standardUnitsContext.Minute);
            Quantity distance = (speed * time).ConvertTo(_standardUnitsContext.Kilometer, 4);
            distance.Value.Should().Be( 30,  $"equal to {speed.Value}{speed.Unit.Symbol} times {time.Value}{time.Unit.Symbol}" );
            distance.Unit.Name.Should().Be( _standardUnitsContext.Kilometer.Name );
            (speed * time).Unit.Symbol.Should().Be( $"{_standardUnitsContext.Kilometer}.{_standardUnitsContext.Minute}.{_standardUnitsContext.Hour.Invert()}" ,
                                                    $"is the product of {speed} and {time}" );
            Quantity timeSquared = time * time * time;
            timeSquared.Value.Should().Be( 15 * 15 * 15 );
            timeSquared.Unit.Symbol.Should().Be( $"{_standardUnitsContext.Minute}3");
        }

        /// <summary>   Tests quantity casting. </summary>
        [Test]
        public static void QuantityCastingTest()
        {
            Quantity a = (Quantity)350.0;
            a.Should().Be( new Quantity(350.0, MeasureUnit.None));

            Quantity b = new Quantity(123.0, MeasureUnit.None);
            ((double)b).Should().Be( 123.0 );

            ((Quantity)15.3).ToString().Should().Be( "15.3" );

        }

        [Test]
        public static void DimensionlessOutcomeTest()
        {
            Quantity GramsPerKilogram = 1.WithUnit( MeasureUnit.Kilogram ) / 1.WithUnit( MeasureUnit.Gram );
            GramsPerKilogram.Unit.Should().Be( MeasureUnit.None, $"{MeasureUnit.Kilogram } / {MeasureUnit.Gram} is dimensionless" );
            (GramsPerKilogram).Value.Should().Be( 1000 , $"there are 1000 {MeasureUnit.Gram} in one {MeasureUnit.Kilogram}" );

            Quantity a = new Quantity( 2.0, _standardUnitsContext.Meter );
            Quantity b = new Quantity( 17.0, MeasureStandardPrefix.Centi[_standardUnitsContext.Meter] );
            Quantity p = (b / a).ConvertTo( _standardUnitsContext.Percent );
            p.ToString( "0.00 US", CultureInfo.InvariantCulture ).Should().Be( "8.50 %" );
            (( double )p).Should().Be( 0.085 );
        }

        /// <summary>   Tests quantity canonical. </summary>
        [Test]
        public static void QuantityCanonicalTest()
        {
            Quantity a = new Quantity( 300, _standardUnitsContext.Mile / _standardUnitsContext.Hour.Power( 2 ) );
            Assert.IsTrue( MeasureUnit.AreCanonical( a.Unit , _standardUnitsContext.Meter / _standardUnitsContext.Second.Power( 2 ) ) );
            Assert.IsTrue( MeasureUnit.AreCanonical( a.Unit, _standardUnitsContext.Meter * _standardUnitsContext.Second.Power( -2 ) ) );
            Assert.IsFalse( MeasureUnit.AreCanonical( a.Unit, _standardUnitsContext.Meter / _standardUnitsContext.Second.Power( 1 ) ) );
            Assert.IsFalse( MeasureUnit.AreCanonical( a.Unit, _standardUnitsContext.Gram ) );
        }

        /// <summary>   Tests conversions to <see cref="MeasureUnit.None"/>. </summary>
        ///
        /// <remarks>
        /// This test passed after changing
        /// <see cref="MeasureUnit.CanConvertTo(MeasureUnit, MeasureUnit)"/> to ignore the context
        /// equality constraint when the target unit is <see cref="MeasureUnit.None"/>, which context is
        /// Null.
        /// </remarks>
        [Test]
        public static void ConvertingToNoneTest()
        {
            var percent = MeasureUnit.DefineAlias( "%", "Percent", new ExpFactor( 0, -2 ), MeasureUnit.None );
            var pc10 = 10.WithUnit( percent );
            pc10.ToString().Should().Be( "10 %" );
            // Message: Expected pc10.CanConvertTo( MeasureUnit.None ) to be True because Percent should be convertible to None, but found False.
            pc10.CanConvertTo( MeasureUnit.None ).Should().Be( true, $"{percent.Name} should be convertible to {MeasureUnit.None.Name}" );
            (pc10.ConvertTo( MeasureUnit.None ).Value).Should().Be( 0.01 * pc10.Value, $"is {pc10.ToString()}" );
        }

        /// <summary>   Tests dimensionless percent operations. </summary>
        [Test]
        public static void DimensionlessPercentOperationsTest()
        {
            MeasureUnit percent = MeasureUnit.DefineAlias( "%", "Percent", new ExpFactor( 0, -2 ), MeasureUnit.None );
            Quantity pc10 = 10.WithUnit( percent );
            Quantity v100 = 100.WithUnit( MeasureUnit.None );
            Quantity pc10of100 = (pc10 * v100);
            pc10of100.Value.Should().Be( 0.01 * pc10.Value * v100.Value, $"is {pc10.ToString()} of {v100.ToString()}" );
            pc10of100.Unit.Symbol.Should().Be( MeasureUnit.NoneSymbol, $"is the symbol of {nameof( MeasureUnit.None )}" );
            Quantity v100Timespc10 = v100 * pc10;
            v100Timespc10.Value.Should().Be( 0.01 * pc10.Value * v100.Value, $"is {v100.ToString()} times {pc10.ToString()}" );
            v100Timespc10.Unit.Symbol.Should().Be( MeasureUnit.NoneSymbol, $"is the symbol of {nameof( MeasureUnit.None )}" );
            int factor = 100;
            Quantity pc1000 = factor * pc10 ;
            pc1000.Value.Should().Be( factor * pc10.Value , $"is {factor} times {pc10.ToString()}" );
            pc1000.Unit.Symbol.Should().Be( pc10.Unit.Symbol, $"is the symbol of {nameof( percent )}" );
        }

        /// <summary>   Tests unit percent operations. </summary>
        [Test]
        public static void UnitPercentOperationsTest()
        {
            Quantity prcnt = new Quantity( 15.0, _standardUnitsContext.Percent );
            Quantity minutes = new Quantity( 300.0, _standardUnitsContext.Minute );

            prcnt.ToString( $"0 {Quantity.FormatCustomUnitSymbol}" ).Should().Be( "15 %" );

            ((double)prcnt).Should().Be( 0.01 * prcnt.Value , $"is {prcnt.ToString( $"0 {Quantity.FormatCustomUnitSymbol}" )}" );
            Quantity percentOfMinutes = prcnt * minutes;
            percentOfMinutes.Unit.Symbol.Should().Be(  $"10^-2.{minutes.Unit.Symbol}",
                                                        $"is the unit of the product of {prcnt.ToString()} and {minutes.ToString()}" );
            percentOfMinutes.Value.Should().Be( prcnt.Value * minutes.Value,
                                                        $"is the product of {prcnt.ToString()} and {minutes.ToString()} {percentOfMinutes.Unit} " );
            percentOfMinutes.ConvertTo(_standardUnitsContext.Minute).Value.Should().Be( 0.01 * prcnt.Value * minutes.Value,
                                                        $"is the product of {prcnt.ToString()} and {minutes.ToString()} {percentOfMinutes.Unit} " );

            Quantity minutesPercent = minutes * prcnt;
            minutesPercent.Unit.Symbol.Should().Be( $"10^-2.{minutes.Unit.Symbol}",
                                                        $"is the unit of the product of {minutes.ToString()} and {prcnt.ToString()}" );
            minutesPercent.Value.Should().Be( prcnt.Value * minutes.Value,
                                                        $"is the product of {minutes.ToString()} and {prcnt.ToString()} {minutesPercent.Unit} " );
            minutesPercent.ConvertTo( _standardUnitsContext.Minute ).Value.Should().Be( 0.01 * prcnt.Value * minutes.Value,
                                                        $"is the product of {minutes.ToString()} and {prcnt.ToString()} {minutesPercent.Unit} " );
        }

        [Test]
        public static void QuantityPowerTest()
        {
            Quantity a = new Quantity( 12.0, _standardUnitsContext.Meter );
            (a.Power( 0 )).Should().Be( 1.WithUnit( MeasureUnit.None ) );
            (a.Power( 1 )).Should().Be( 12.WithUnit( _standardUnitsContext.Meter ) );
            (a.Power( 2 )).Should().Be( 144.WithUnit( _standardUnitsContext.SquareMeter ) );
            (a.Power( 3 )).Should().Be( 1728.WithUnit( _standardUnitsContext.CubicMeter ) );
            (a.Power( -1 )).Should().Be( (1.0 / 12).WithUnit( MeasureUnit.None / _standardUnitsContext.Meter ) );
            (a.Power( -2 )).Should().Be( (1.0 / 144).WithUnit( MeasureUnit.None / _standardUnitsContext.SquareMeter ) );
            (a.Power( -3 )).Should().Be( (1.0 / 1728).WithUnit( MeasureUnit.None / _standardUnitsContext.CubicMeter ) );
        }

        /// <summary>   Tests division by zero. </summary>
        [Test]
        public static void DivisionByZeroTest()
        {
            Quantity d1 = new Quantity( 32.0, _standardUnitsContext.Kilometer );
            Quantity d2 = new Quantity( 0.0, _standardUnitsContext.Kilometer );
            Quantity t = new Quantity( 0.0, _standardUnitsContext.Hour );

            Quantity s;

            s = d1 / t;

            Assert.IsTrue( Double.IsInfinity( s.Value ) );
            Assert.IsTrue( Double.IsPositiveInfinity( s.Value ) );
            Assert.AreEqual( s.Unit, (d1.Unit / t.Unit) );

            s = d2 / t;

            Assert.IsTrue( Double.IsNaN( s.Value ) );
            Assert.AreEqual( s.Unit, (d2.Unit / t.Unit) );
        }

        #endregion Quantity Math

        #region . Comparisons .

        /// <summary>   Tests compare speed units. </summary>
        [Test]
        public static void CompareSpeedUnitsTest()
        {
            Quantity a = new Quantity( 120.0, _standardUnitsContext.KilometerPerHour );
            Quantity b = new Quantity( 33.3333333333333,  _standardUnitsContext.Meter / _standardUnitsContext.Second );

            (a.ToNormalizedString()).Should().Be( b.ToNormalizedString() );

            a.Should().Be( b );
            Assert.IsFalse( a < b );
            Assert.IsFalse( a > b );
            Assert.IsTrue( a <= b );
            Assert.IsTrue( a >= b );
            Assert.IsFalse( a != b );
        }

        /// <summary>   Tests compare energy units. </summary>
        [Test]
        public static void CompareEnergyUnitsTest()
        {
            Quantity a = new Quantity( -0.00002, _standardUnitsContext.Horsepower );
            Quantity b = new Quantity( -0.00002, _standardUnitsContext.Horsepower );

            Quantity ar = a.ConvertTo( _standardUnitsContext.Watt );
            Quantity br = b.ConvertTo( _standardUnitsContext.Watt );

            Assert.IsTrue( a == b );
            Assert.IsFalse( a > b );
            Assert.IsFalse( a < b );
            Assert.IsTrue( ar == br );
            Assert.IsFalse( ar > br );
            Assert.IsFalse( ar < br );
        }

        /// <summary>   Tests rounded comparison. </summary>
        [Test]
        public static void RoundedComparisonTest()
        {
            Quantity a = new Quantity( 0.045, _standardUnitsContext.Meter );
            Quantity b = new Quantity( 0.04500000000000001, _standardUnitsContext.Meter );
            Quantity c = new Quantity( 0.04500000000000002, _standardUnitsContext.Meter );
            Quantity d = new Quantity( 0.046, _standardUnitsContext.Meter );
            Assert.IsFalse( a.Value == b.Value );
            Assert.IsFalse( b.Value == c.Value );
            Assert.IsFalse( a.Value == c.Value );
            a.Should().Be( b);
            b.Should().Be( c );
            a.Should().Be( c );
            c.Should().NotBe( d );
            Assert.IsTrue( a.Equals( b ) );
            Assert.IsTrue( b.Equals( c ) );
            Assert.IsTrue( a.Equals( c ) );
            Assert.IsFalse( c.Equals( d ) );
        }


        #endregion

        #region . Formatting .


        /// <summary>   Tests static formatting. </summary>
        [Test]
        public static void StaticFormattingTest()
        {
            Quantity a = new Quantity( 1234.5678, _standardUnitsContext.Meter );

            CultureInfo enus = CultureInfo.GetCultureInfo( "en-US" );
            CultureInfo nlbe = CultureInfo.GetCultureInfo( "nl-BE" );

            Assert.AreEqual( "1234.5678 m", Quantity.ToString( a ).Replace( ",", "." ) );
            Assert.AreEqual( "1234.5678 m", Quantity.ToString( a, enus ) );
            Assert.AreEqual( "1234,5678 m", Quantity.ToString( a, nlbe ) );
            Assert.AreEqual( "1.234.57 m", Quantity.ToString( a, "#,##0.00 US" ).Replace( ",", "." ) );
            Assert.AreEqual( "1,234.57 m", Quantity.ToString( a, "#,##0.00 US", enus ) );
            Assert.AreEqual( "1.234,57 m", Quantity.ToString( a, "#,##0.00 US", nlbe ) );
        }

        /// <summary>   Tests format code. </summary>
        [Test]
        public static void FormatCodeTest()
        {
            CultureInfo defaultCultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

                CultureInfo nlbe = CultureInfo.GetCultureInfo( "nl-BE" );
                CultureInfo enus = CultureInfo.GetCultureInfo( "en-US" );

                Quantity a = new Quantity( 12.3456789, _standardUnitsContext.Kilometer );
                Quantity b = new Quantity( 12345.6789, _standardUnitsContext.Meter );
                Quantity c = new Quantity( -0.45, _standardUnitsContext.Kilometer / _standardUnitsContext.Hour );
                Quantity d = new Quantity( 25.678, _standardUnitsContext.Meter * _standardUnitsContext.Meter );

                Assert.AreEqual( "12.3456789 km", a.ToString() );
                Assert.AreEqual( "12,3456789 Kilometer", a.ToString( "GN", nlbe ) );
                Assert.AreEqual( "12.346 km", a.ToString( "0.000 US", enus ) );
                Assert.AreEqual( "12.346 km", a.ToString( "0.000 US", CultureInfo.CurrentUICulture ) );
                Assert.AreEqual( "12.346", a.ToString( "0.000", CultureInfo.CurrentUICulture ) );
                Assert.AreEqual( "12,35 km", a.ToString( "NS", nlbe ) );
                Assert.AreEqual( "12.35 km", a.ToString( "NS", enus ) );
                Assert.AreEqual( "12.345,68 m", b.ToString( "NS", nlbe ) );
                Assert.AreEqual( "12,345.68 m", b.ToString( "NS", enus ) );
                Assert.AreEqual( "-0.45 km.h-1", c.ToString( "NS", enus ) );
                Assert.AreEqual( "-0.45 Kilometer.Hour^-1", c.ToString( "NN", enus ) );
                Assert.AreEqual( "-0,450 km.h-1", c.ToString( "0.000 US", nlbe ) );
                Assert.AreEqual( "[0,450] km.h-1", c.ToString( "0.000 US;[0.000] US", nlbe ) );

                Assert.AreEqual( "12.35 Kilometer", b.ToString( "NN|kilometer" ) );
                Assert.AreEqual( "12.346 km", b.ToString( "#,##0.000 US|kilometer" ) );
                Assert.AreEqual( "+12.346 km", b.ToString( "+#,##0.000 US|kilometer" ) );
                Assert.AreEqual( "12.346 km neg", (-b).ToString( "#,##0.000 US pos;#,##0.000 US neg|kilometer" ) );

                Assert.AreEqual( "25.68 m2", d.ToString( "NS" ) );

                Quantity e = new Quantity( 1234.5678, _standardUnitsContext.Meter );
                Assert.AreEqual( "1,234.568 Meter", Quantity.ToString( e, "#,##0.000 UN", CultureInfo.InvariantCulture ) );
            }
            finally
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = defaultCultureInfo;
            }
        }

        /// <summary>   Tests composite format. </summary>
        [Test]
        public static void CompositeFormatTest()
        {
            Quantity d = new Quantity( 278.9, _standardUnitsContext.Mile );
            Quantity t = new Quantity( 2.5, _standardUnitsContext.Hour );

            var s = d / t;

            // this test can only be used with registered static unit properties
            Assert.AreEqual(
                "Taking 2.5 h to travel 449 km means your speed was 179.54 km.h-1",
                String.Format( "Taking {1:GS|hour} to travel {0:#,##0 US|kilometer} means your speed was {2:#,##0.00 US|Kilometer.Hour^-1}", d, t, s ) );

            Quantity? a = null;

            Assert.AreEqual( "a = ", String.Format( "a = {0:#,##0.0 US}", a ) );
        }

        /// <summary>   Tests custom format sting. </summary>
        [Test]
        public static void CustomFormatStingTest()
        {
            CultureInfo defaultCultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                Quantity a = new Quantity( 12.3456789, _standardUnitsContext.Kilometer );
                Assert.AreEqual( "12.346 km", a.ToString( "{0:G5} {1}", CultureInfo.CurrentUICulture ) );
                Assert.AreEqual( "12.346", a.ToString( "{0:G5}", CultureInfo.CurrentUICulture ) );
            }
            finally
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = defaultCultureInfo;
            }
        }

#endregion Formatting

#region . Split Tests .

        /// <summary>   Tests split time unit. </summary>
        [Test]
        public static void SplitTimeUnitTest()
        {
            Quantity a = new Quantity( 146.0, _standardUnitsContext.Second );
            Quantity[] values = a.Split( new MeasureUnit[] { _standardUnitsContext.Hour, _standardUnitsContext.Minute, _standardUnitsContext.Second }, 0 );

            Assert.AreEqual( 3, values.Length );
            Assert.AreEqual( new Quantity( 0.0, _standardUnitsContext.Hour ), values[0] );
            Assert.AreEqual( new Quantity( 2.0, _standardUnitsContext.Minute ), values[1] );
            Assert.AreEqual( new Quantity( 26.0, _standardUnitsContext.Second ), values[2] );
        }

        /// <summary>   Tests split metric to imperial units. </summary>
        [Test]
        public static void SplitMetricToImperialUnitsTest()
        {
            Quantity a = new Quantity( 10.5, _standardUnitsContext.Meter );
            Quantity[] values = a.Split( new MeasureUnit[] { _standardUnitsContext.Yard, _standardUnitsContext.Foot, _standardUnitsContext.Inch }, 1 );

            string separator = "";
            foreach( Quantity v in values )
            {
                Console.Write( separator );
                Console.Write( v );
                separator = ", ";
            }
            Console.WriteLine();

            Assert.AreEqual( 3, values.Length );
            Assert.AreEqual( new Quantity( 11.0, _standardUnitsContext.Yard ), values[0] );
            Assert.AreEqual( new Quantity( 1.0, _standardUnitsContext.Foot ), values[1] );
            Assert.AreEqual( new Quantity( 5.4, _standardUnitsContext.Inch ), values[2] );
        }

        /// <summary>   Tests split metric to metric. </summary>
        [Test]
        public static void SplitMetricToMetricTest()
        {
            Quantity a = new Quantity( System.Math.Sqrt( 13 ), _standardUnitsContext.Meter );
            Quantity[] values = a.Split( new MeasureUnit[] { _standardUnitsContext.Meter, _standardUnitsContext.Decimeter,
                                                      MeasureStandardPrefix.Centi[_standardUnitsContext.Meter],
                                                      MeasureStandardPrefix.Milli[_standardUnitsContext.Meter] }, 0 );
            Assert.AreEqual( new Quantity( 3.0, _standardUnitsContext.Meter ), values[0] );
            Assert.AreEqual( new Quantity( 6.0, _standardUnitsContext.Decimeter ), values[1] );
            Assert.AreEqual( new Quantity( 0.0, MeasureStandardPrefix.Centi[_standardUnitsContext.Meter] ), values[2] );
            Assert.AreEqual( new Quantity( 6.0, MeasureStandardPrefix.Milli[_standardUnitsContext.Meter] ), values[3] );
        }

        /// <summary>   Tests week split. </summary>
        [Test]
        public static void WeekSplitTest()
        {
            // One fifth of a week:
            Quantity a = (0.2 * 24.0 * 7.0).WithUnit( _standardUnitsContext.Hour) ;
            Quantity[] result = a.Split( new MeasureUnit[] { _standardUnitsContext.Day, _standardUnitsContext.Hour,
                                                             _standardUnitsContext.Minute, _standardUnitsContext.Second }, 3 );
            Assert.AreEqual( 4, result.Length );
            CollectionAssert.AreEqual( new double[] { 1.0, 9.0, 36.0, 0.0 }.ToList(), result.Select( x => x.Value ).ToList() );
            // the split results in 1 day, 9 hours, 35 minutes and 59.99999 seconds rounder to 60!
        }

#endregion

    }
}

