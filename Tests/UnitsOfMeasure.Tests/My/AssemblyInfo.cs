﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle("Invenietis Units of Measure Test Library")]
[assembly: AssemblyProduct("CK.Units.of.Measure.Tests")]
[assembly: AssemblyDescription("Invenietis Units of Measure Test library")]
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
